webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<app-navbar></app-navbar>\n\n<!-- \"contenido\" es una clases con el padding-top: 120px para no tapar el contenido al fijar el navbar-->\n\n\n  <router-outlet></router-outlet>\n\n\n<div id=\"conta_neura\">\n  <app-footer></app-footer>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html")
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng_recaptcha__ = __webpack_require__("../../../../ng-recaptcha/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng_recaptcha___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ng_recaptcha__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_routes__ = __webpack_require__("../../../../../src/app/app.routes.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__servicios_talks_service__ = __webpack_require__("../../../../../src/app/servicios/talks.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__servicios_stories_service__ = __webpack_require__("../../../../../src/app/servicios/stories.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_navbar_navbar_component__ = __webpack_require__("../../../../../src/app/components/navbar/navbar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_home_home_component__ = __webpack_require__("../../../../../src/app/components/home/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_about_about_component__ = __webpack_require__("../../../../../src/app/components/about/about.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_services_services_component__ = __webpack_require__("../../../../../src/app/components/services/services.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_stories_stories_component__ = __webpack_require__("../../../../../src/app/components/stories/stories.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_talks_talks_component__ = __webpack_require__("../../../../../src/app/components/talks/talks.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_contact_contact_component__ = __webpack_require__("../../../../../src/app/components/contact/contact.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_footer_footer_component__ = __webpack_require__("../../../../../src/app/components/footer/footer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__components_stories_storie_storie_component__ = __webpack_require__("../../../../../src/app/components/stories/storie/storie.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__components_talks_talk_talk_component__ = __webpack_require__("../../../../../src/app/components/talks/talk/talk.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__components_services_branding_branding_component__ = __webpack_require__("../../../../../src/app/components/services/branding/branding.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__components_services_product_product_component__ = __webpack_require__("../../../../../src/app/components/services/product/product.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__components_services_tech_tech_component__ = __webpack_require__("../../../../../src/app/components/services/tech/tech.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__components_privacy_privacy_component__ = __webpack_require__("../../../../../src/app/components/privacy/privacy.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__components_stories_storie_temika_temika_component__ = __webpack_require__("../../../../../src/app/components/stories/storie/temika/temika.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__components_stories_storie_spiglocase_spiglocase_component__ = __webpack_require__("../../../../../src/app/components/stories/storie/spiglocase/spiglocase.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__components_stories_storie_sahlo_sahlo_component__ = __webpack_require__("../../../../../src/app/components/stories/storie/sahlo/sahlo.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__components_stories_storie_metalplex_metalplex_component__ = __webpack_require__("../../../../../src/app/components/stories/storie/metalplex/metalplex.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__components_stories_storie_bitlegends_bitlegends_component__ = __webpack_require__("../../../../../src/app/components/stories/storie/bitlegends/bitlegends.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__components_stories_storie_virtef_virtef_component__ = __webpack_require__("../../../../../src/app/components/stories/storie/virtef/virtef.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__components_stories_storie_arnic_arnic_component__ = __webpack_require__("../../../../../src/app/components/stories/storie/arnic/arnic.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__components_stories_storie_copival_copival_component__ = __webpack_require__("../../../../../src/app/components/stories/storie/copival/copival.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__components_stories_storie_tenorio_tenorio_component__ = __webpack_require__("../../../../../src/app/components/stories/storie/tenorio/tenorio.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__components_stories_storie_lakshmi_lakshmi_component__ = __webpack_require__("../../../../../src/app/components/stories/storie/lakshmi/lakshmi.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__components_stories_storie_mm_mm_component__ = __webpack_require__("../../../../../src/app/components/stories/storie/mm/mm.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// Rutas

// Servicios


// Componentes


























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_7__components_navbar_navbar_component__["a" /* NavbarComponent */],
                __WEBPACK_IMPORTED_MODULE_8__components_home_home_component__["a" /* HomeComponent */],
                __WEBPACK_IMPORTED_MODULE_9__components_about_about_component__["a" /* AboutComponent */],
                __WEBPACK_IMPORTED_MODULE_10__components_services_services_component__["a" /* ServicesComponent */],
                __WEBPACK_IMPORTED_MODULE_11__components_stories_stories_component__["a" /* StoriesComponent */],
                __WEBPACK_IMPORTED_MODULE_12__components_talks_talks_component__["a" /* TalksComponent */],
                __WEBPACK_IMPORTED_MODULE_13__components_contact_contact_component__["a" /* ContactComponent */],
                __WEBPACK_IMPORTED_MODULE_14__components_footer_footer_component__["a" /* FooterComponent */],
                __WEBPACK_IMPORTED_MODULE_15__components_stories_storie_storie_component__["a" /* StorieComponent */],
                __WEBPACK_IMPORTED_MODULE_16__components_talks_talk_talk_component__["a" /* TalkComponent */],
                __WEBPACK_IMPORTED_MODULE_17__components_services_branding_branding_component__["a" /* BrandingComponent */],
                __WEBPACK_IMPORTED_MODULE_18__components_services_product_product_component__["a" /* ProductComponent */],
                __WEBPACK_IMPORTED_MODULE_19__components_services_tech_tech_component__["a" /* TechComponent */],
                __WEBPACK_IMPORTED_MODULE_20__components_privacy_privacy_component__["a" /* PrivacyComponent */],
                __WEBPACK_IMPORTED_MODULE_21__components_stories_storie_temika_temika_component__["a" /* TemikaComponent */],
                __WEBPACK_IMPORTED_MODULE_22__components_stories_storie_spiglocase_spiglocase_component__["a" /* SpiglocaseComponent */],
                __WEBPACK_IMPORTED_MODULE_23__components_stories_storie_sahlo_sahlo_component__["a" /* SahloComponent */],
                __WEBPACK_IMPORTED_MODULE_24__components_stories_storie_metalplex_metalplex_component__["a" /* MetalplexComponent */],
                __WEBPACK_IMPORTED_MODULE_30__components_stories_storie_lakshmi_lakshmi_component__["a" /* LakshmiComponent */],
                __WEBPACK_IMPORTED_MODULE_29__components_stories_storie_tenorio_tenorio_component__["a" /* TenorioComponent */],
                __WEBPACK_IMPORTED_MODULE_28__components_stories_storie_copival_copival_component__["a" /* CopivalComponent */],
                __WEBPACK_IMPORTED_MODULE_27__components_stories_storie_arnic_arnic_component__["a" /* ArnicComponent */],
                __WEBPACK_IMPORTED_MODULE_26__components_stories_storie_virtef_virtef_component__["a" /* VirtefComponent */],
                __WEBPACK_IMPORTED_MODULE_25__components_stories_storie_bitlegends_bitlegends_component__["a" /* BitlegendsComponent */],
                __WEBPACK_IMPORTED_MODULE_31__components_stories_storie_mm_mm_component__["a" /* MmComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__app_routes__["a" /* APP_ROUTING */],
                __WEBPACK_IMPORTED_MODULE_2_ng_recaptcha__["RecaptchaModule"].forRoot(),
            ],
            // En providers van todos los servicios
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__servicios_talks_service__["a" /* TalksService */],
                __WEBPACK_IMPORTED_MODULE_5__servicios_stories_service__["a" /* StoriesService */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/app.routes.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return APP_ROUTING; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_home_home_component__ = __webpack_require__("../../../../../src/app/components/home/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_about_about_component__ = __webpack_require__("../../../../../src/app/components/about/about.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_services_services_component__ = __webpack_require__("../../../../../src/app/components/services/services.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_stories_stories_component__ = __webpack_require__("../../../../../src/app/components/stories/stories.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_talks_talks_component__ = __webpack_require__("../../../../../src/app/components/talks/talks.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_contact_contact_component__ = __webpack_require__("../../../../../src/app/components/contact/contact.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_stories_storie_storie_component__ = __webpack_require__("../../../../../src/app/components/stories/storie/storie.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_stories_storie_arnic_arnic_component__ = __webpack_require__("../../../../../src/app/components/stories/storie/arnic/arnic.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_stories_storie_bitlegends_bitlegends_component__ = __webpack_require__("../../../../../src/app/components/stories/storie/bitlegends/bitlegends.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_stories_storie_copival_copival_component__ = __webpack_require__("../../../../../src/app/components/stories/storie/copival/copival.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_stories_storie_lakshmi_lakshmi_component__ = __webpack_require__("../../../../../src/app/components/stories/storie/lakshmi/lakshmi.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_stories_storie_metalplex_metalplex_component__ = __webpack_require__("../../../../../src/app/components/stories/storie/metalplex/metalplex.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_stories_storie_mm_mm_component__ = __webpack_require__("../../../../../src/app/components/stories/storie/mm/mm.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_stories_storie_sahlo_sahlo_component__ = __webpack_require__("../../../../../src/app/components/stories/storie/sahlo/sahlo.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__components_stories_storie_spiglocase_spiglocase_component__ = __webpack_require__("../../../../../src/app/components/stories/storie/spiglocase/spiglocase.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__components_stories_storie_temika_temika_component__ = __webpack_require__("../../../../../src/app/components/stories/storie/temika/temika.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__components_stories_storie_tenorio_tenorio_component__ = __webpack_require__("../../../../../src/app/components/stories/storie/tenorio/tenorio.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__components_stories_storie_virtef_virtef_component__ = __webpack_require__("../../../../../src/app/components/stories/storie/virtef/virtef.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__components_talks_talk_talk_component__ = __webpack_require__("../../../../../src/app/components/talks/talk/talk.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__components_services_branding_branding_component__ = __webpack_require__("../../../../../src/app/components/services/branding/branding.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__components_services_tech_tech_component__ = __webpack_require__("../../../../../src/app/components/services/tech/tech.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__components_services_product_product_component__ = __webpack_require__("../../../../../src/app/components/services/product/product.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__components_privacy_privacy_component__ = __webpack_require__("../../../../../src/app/components/privacy/privacy.component.ts");
























var APP_ROUTES = [
    { path: 'home', component: __WEBPACK_IMPORTED_MODULE_1__components_home_home_component__["a" /* HomeComponent */] },
    { path: 'privacy', component: __WEBPACK_IMPORTED_MODULE_23__components_privacy_privacy_component__["a" /* PrivacyComponent */] },
    { path: 'about', component: __WEBPACK_IMPORTED_MODULE_2__components_about_about_component__["a" /* AboutComponent */] },
    { path: 'services', component: __WEBPACK_IMPORTED_MODULE_3__components_services_services_component__["a" /* ServicesComponent */] },
    { path: 'stories', component: __WEBPACK_IMPORTED_MODULE_4__components_stories_stories_component__["a" /* StoriesComponent */] },
    // children: STORIES_ROUTES },
    { path: 'arnic', component: __WEBPACK_IMPORTED_MODULE_8__components_stories_storie_arnic_arnic_component__["a" /* ArnicComponent */] },
    { path: 'bitlegends', component: __WEBPACK_IMPORTED_MODULE_9__components_stories_storie_bitlegends_bitlegends_component__["a" /* BitlegendsComponent */] },
    { path: 'copival', component: __WEBPACK_IMPORTED_MODULE_10__components_stories_storie_copival_copival_component__["a" /* CopivalComponent */] },
    { path: 'lakshmi', component: __WEBPACK_IMPORTED_MODULE_11__components_stories_storie_lakshmi_lakshmi_component__["a" /* LakshmiComponent */] },
    { path: 'metalplex', component: __WEBPACK_IMPORTED_MODULE_12__components_stories_storie_metalplex_metalplex_component__["a" /* MetalplexComponent */] },
    { path: 'mm', component: __WEBPACK_IMPORTED_MODULE_13__components_stories_storie_mm_mm_component__["a" /* MmComponent */] },
    { path: 'sahlo', component: __WEBPACK_IMPORTED_MODULE_14__components_stories_storie_sahlo_sahlo_component__["a" /* SahloComponent */] },
    { path: 'spiglocase', component: __WEBPACK_IMPORTED_MODULE_15__components_stories_storie_spiglocase_spiglocase_component__["a" /* SpiglocaseComponent */] },
    { path: 'temika', component: __WEBPACK_IMPORTED_MODULE_16__components_stories_storie_temika_temika_component__["a" /* TemikaComponent */] },
    { path: 'tenorio', component: __WEBPACK_IMPORTED_MODULE_17__components_stories_storie_tenorio_tenorio_component__["a" /* TenorioComponent */] },
    { path: 'virtef', component: __WEBPACK_IMPORTED_MODULE_18__components_stories_storie_virtef_virtef_component__["a" /* VirtefComponent */] },
    { path: 'storie/:id', component: __WEBPACK_IMPORTED_MODULE_7__components_stories_storie_storie_component__["a" /* StorieComponent */] },
    { path: 'talks', component: __WEBPACK_IMPORTED_MODULE_5__components_talks_talks_component__["a" /* TalksComponent */] },
    { path: 'contact', component: __WEBPACK_IMPORTED_MODULE_6__components_contact_contact_component__["a" /* ContactComponent */] },
    { path: 'tech', component: __WEBPACK_IMPORTED_MODULE_21__components_services_tech_tech_component__["a" /* TechComponent */] },
    { path: 'product', component: __WEBPACK_IMPORTED_MODULE_22__components_services_product_product_component__["a" /* ProductComponent */] },
    { path: 'branding', component: __WEBPACK_IMPORTED_MODULE_20__components_services_branding_branding_component__["a" /* BrandingComponent */] },
    { path: 'talk/:id', component: __WEBPACK_IMPORTED_MODULE_19__components_talks_talk_talk_component__["a" /* TalkComponent */] },
    { path: '**', pathMatch: 'full', redirectTo: 'home' }
];
var APP_ROUTING = __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* RouterModule */].forRoot(APP_ROUTES, { useHash: true });


/***/ }),

/***/ "../../../../../src/app/components/about/about.component.html":
/***/ (function(module, exports) {

module.exports = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>\n  <img src=\"assets/img/neura_loading.gif\" width=\"15%\" class=\"loanding\" alt=\"LOANDING\">\n"

/***/ }),

/***/ "../../../../../src/app/components/about/about.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AboutComponent = /** @class */ (function () {
    function AboutComponent() {
    }
    AboutComponent.prototype.ngOnInit = function () {
    };
    AboutComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-about',
            template: __webpack_require__("../../../../../src/app/components/about/about.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], AboutComponent);
    return AboutComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/contact/contact.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  .\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/components/contact/contact.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ContactComponent = /** @class */ (function () {
    function ContactComponent() {
    }
    ContactComponent.prototype.ngOnInit = function () {
    };
    ContactComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-contact',
            template: __webpack_require__("../../../../../src/app/components/contact/contact.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], ContactComponent);
    return ContactComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/footer/footer.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"footer_neura\">\n<div class=\"contact\">\n<div class=\"container\">\n  <h4>CONTACTO</h4>\n\n  <div class=\"text-white text-justify\" style=\"font-size: 0.7em; width: 40%;\">\n    Si deseas recibir más información sobre alguno\n    de nuestros servicios, o tienes una idea \n    que te gustaría desarrollar no dudes en contactarnos\n     y con gusto te ayudamos.\n  </div>\n\n<div class=\"row\">\n<div class=\"col-md-6\">\n\n  <!-- <p>Si deseas recibir más información sobre alguno de nuestros servicios, o tienes una idea que te gustaría desarrollar no dudes en contactarnos y con gusto te ayudaremos.</p> --> <br/><br/>\n  <p class=\"text-center text-md-left text-sm-center\"><a style=\"color: white;\" href=\"mailto:contacto@neuracorestudio.com\">contacto@neuracorestudio.com</a>\n    <br><a style=\"color: white;\" href=\"tel://5540652122\">55 40 65 21 22</a><br><a style=\"color: white;\" href=\"tel://2721295986\"> 27 21 29 59 86</a></p>\n</div>\n\n<div class=\"col-md-6\">\n  <form action=\"form_mail.php\" method=\"post\" data-form-title=\"CONTACTANOS\">\n    <input type=\"hidden\" data-form-email=\"true\">\n      <div class=\"row row-sm-offset\">\n        <div class=\"col-md-12\">\n          <div class=\"form-group\">\n            <input type=\"text\" class=\"input_neura form-control\" name=\"nombre\" placeholder=\"\" required=\"\" >\n            <label class=\"\" for=\"form1-4-name\">NOMBRE<span class=\"form-asterisk\">*</span></label>\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-md-6\">\n          <div class=\"form-group\">\n            <input type=\"email\" class=\"input_neura form-control\" name=\"email_address\" placeholder=\"\" required=\"\" >\n            <label class=\"\" for=\"form1-4-email\">CORREO<span class=\"form-asterisk\">*</span></label>\n          </div>\n        </div>\n        <div class=\"col-xs-12 col-md-6\">\n          <div class=\"form-group\">\n            <input type=\"tel\" class=\"input_neura form-control\" name=\"telefono\" placeholder=\"\" >\n            <label class=\"\" for=\"form1-4-phone\">TELÉFONO</label>\n          </div>\n        </div>\n        <div class=\"col-md-12\">\n        <div class=\"form-group\">\n          <textarea class=\"input_neura form-control\" name=\"comentarios\" placeholder=\"\" rows=\"2\"></textarea>\n          <label class=\"\" >MENSAJE</label>\n        </div>\n        </div>\n\n      </div>\n\n      <div class=\"row cont_item \">\n    <div class=\"col-auto\">\n      <!-- <div class=\"g-recaptcha\" required=\"\" data-sitekey=\"6LcqzjsUAAAAAPt5_OGHe9Pa_AGGZRxWfR9ymzBX\"></div> -->\n      <re-captcha (resolved)=\"resolved($event)\" required siteKey=\"6LcqzjsUAAAAAPt5_OGHe9Pa_AGGZRxWfR9ymzBX\"></re-captcha>\n    </div>\n    <div class=\"col-auto\">\n      <button type=\"submit\" class=\"button_enviar \">ENVIAR</button>\n    </div>\n  </div>\n\n\n\n  </form>\n</div>\n  </div>\n  </div>\n</div>\n<br>\n\n<div class=\"container\">\n  <hr style=\"border: 0.5px solid #fff;\">\n</div>\n\n<footer class=\"container\">\n  <div class=\"row\">\n    <div class=\"col-md-6\">\n    <p style=\"color:#fff;\" class=\"text-center text-md-left text-sm-center\">NEURACORE STUDIO<br>&copy; 2018. Todos los derechos reservados.\n      <br><a style=\"color:#fff;\" class=\" nav_li\" [routerLink]=\"['privacy']\">AVISO DE PRIVACIDAD</a></p>\n    </div>\n    <div class=\"col-md-6 redes_soc\">\n      <a href=\"tel://+5215540652122\">\n        <img class=\"red_soc\" width=\"60px\" src=\"../../../../assets/vectors/telefono.svg\" alt=\"telefono\">\n      </a>\n      <a href=\"https://www.facebook.com/neuracorestudio\"target=\"_blank\">\n        <img class=\"red_soc\" width=\"60px\" src=\"../../../../assets/vectors/facebook.svg\" alt=\"facebook\">\n      </a>\n      <a href=\"https://twitter.com/neuracorestudio\" target=\"_blank\">\n        <img class=\"red_soc\" width=\"60px\" src=\"../../../../assets/vectors/twitter.svg\" alt=\"twitter\">\n      </a>\n      <a href=\"https://plus.google.com/b/101634985871492335717/101634985871492335717\" target=\"_blank\">\n        <img class=\"red_soc\" width=\"60px\" src=\"../../../../assets/vectors/googleplus.svg\" alt=\"googleplus\">\n      </a>\n      <a href=\"https://www.instagram.com/neuracorestudio\" target=\"_blank\">\n          <img class=\"red_soc\" width=\"60px\" src=\"../../../../assets/vectors/instagram.svg\" alt=\"instagram\">\n      </a>\n      <a href=\"https://www.linkedin.com/company/neuracore-studio\" target=\"_blank\">\n        <img class=\"red_soc\" width=\"60px\" src=\"../../../../assets/vectors/linkedin.svg\" alt=\"linkedin\">\n      </a>\n    </div>\n  </div>\n\n</footer>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/footer/footer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__("../../../../../src/app/components/footer/footer.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<div  class=\"animated fadeIn\">\n<!-- Potada -->\n<div class=\"container\">\n<div id=\"portadaita\" class=\"portada portada_neura cont_item row \">\n  <div class=\"col-md-5 offset-md-1\">\n    <h2 class=\"mth\">MAKING<br>THINGS<br>HAPPEN</h2>\n  </div>\n  <div class=\"col-md-6\">\n    <p>\n      El mundo está repleto de proyectos e ideas extraordinarias. Sin embargo por distintas razones no\n      son desarrolladas, perdiendo su potencial.\n    </p>\n    <p>\n      <span class=\"neura\">NEURA</span><span class=\"core\">CORE</span> <span class=\"studio\">STUDIO</span> el espacio en donde tus ideas alcanzan todo su potencial.\n    </p>\n    <a class=\"link_neuracore_style col-md-6 offset-md-4 offset-lg-5\" href=\"/#conta_neura\">TENGO&nbsp;UNA&nbsp;IDEA</a>\n  </div>\n</div>\n</div>\n\n<div id=\"nosotros_neura\">\n\n</div>\n\n<br><br><br><br>\n\n<div class=\"container\">\n<div class=\"row\">\n  <div class=\"col-md-5\">\n    <p>Somos una agencia de <span class=\"red\">INNOVACIÓN</span> ubicada en la\n    CIUDAD DE MÉXICO.\n\n    A través del trabajo multidisciplinario ayudamos a\n    empresas, empresarios y emprendedores a conseguir sus\n  metas y hacer realidad sus proyectos.</p> <br><br>\n  </div>\n  <div class=\"col-md-6\">\n\n  </div>\n</div>\n\n</div>\n<!-- End Portada -->\n\n<!-- Banner sin container -->\n<div style= \"background: url(../../../../assets/img/banner_home.png) no-repeat fixed 0px 140px;\n  width: 100%; height: 100%;\n  padding-top: 120px;\n  padding-bottom: 120px;\n  background-size:100%;\"\n  class=\"baner_neura\">\n\n</div>\n\n\n<br><br><br>\n<!--\n<div class=\"jarallax\" data-speed=\"1.4\">\n\n    <div id=\"jarallax-container-4\"\n      style=\"position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; overflow: hidden; pointer-events: none; z-index: -100;\">\n      <img class=\"jarallax-img\" src=\"../../../../assets/img/banner_home.png\" alt=\"\"\n        style=\"object-fit: cover; object-position: 50% 50%; max-width: none; position: fixed; top: 0px; left: 0px; width: 1179px; height: 1160px; overflow: hidden; pointer-events: none; margin-top: -180px; transform: translate3d(0px, -3.10625px, 0px);\">\n    </div></div> -->\n\n<br><br><br>\n\n<div class=\"container\">\n<!-- METODOLOGÍA -->\n  <div class=\"row justify-content-end\">\n\n    <div class=\"col-md-6\">\n      <p>Para garantizar la calidad del servicio, conseguir\n        cumplir con los tiempos de entrega y garantizar los\n        requerimientos de los proyectos hemos desarrollado\n        nuestra <span class=\"red\">METODOLOGÍA</span> que nos permite llegar\n        de una idea a un proyecto final viable.</p> <br> <br>\n    </div>\n  </div>\n\n  <div class=\"row\">\n    <div class=\"col-md-6\">\n      <!-- la clase pull-xs-left no se que sea la neta, se puso sola y la deje :) -->\n      <img src=\"../../../../assets/vectors/metodo_neuracorestudio.svg\" class=\"img-fluid pull-xs-left\" alt=\"...\">\n    </div>\n    <div class=\"col-md-6\">\n\n    </div>\n    </div>\n\n    <div class=\"row justify-content-end\">\n      <div class=\"col-md-6\">\n        <br><br> <p>Todo nuestro proceso es cíclico y está estructurado en etapas que pueden realizarse de manera individual, lo que significa que podemos iniciar en cualquiera de ellas y regresarnos en caso de necesidades particulæs de tu proyecto.</p> <br> <br>\n      </div>\n\n\n  </div>\n\n</div>\n\n<!-- SERVICIOS -->\n<div id=\"servicess\" class=\"container\">\n  <h2 class=\"title_neura\">SERVICIOS</h2>\n  <div class=\"row\">\n    <div class=\"col-md-8\">\n      <p>La innovación es un proceso completo que participa en todas las áreas de una empresa e implica la participación de distintas disciplinas que permiten que el desarrollo de un proyecto sea más sencillo.</p>\n      <p>Para facilitar la implementación de estrategias de innovación en tu empresa contamos con los siguientes servicios para ti.</p>\n    </div>\n  </div>\n\n  <div class=\"row\">\n\n    <div class=\"col-md-4 card_neura\">\n\n      <a class=\"img_card_ser\" href=\"/#/branding\">\n      <img src=\"../../../../assets/img/branding.png\" width=\"100%\" alt=\"\" /> </a>\n      <a class=\"title_card\" href=\"/#/branding\"><h3>BRANDING</h3></a>\n      <a class=\"text_card\" href=\"/#/branding\"><p>Te ayudamos a crear la imagen que te diferenciará de tu competencia, siempre tomando en cuenta las tendencias y mercados globales, de forma que creamos una marca dirigida a tu público objetivo.</p></a>\n      <a class=\"see_more\" href=\"/#/branding\"> <div>+</div> </a>\n    </div>\n    <div class=\"col-md-4 card_neura\">\n      <a class=\"img_card_ser\" href=\"/#/product\">\n        <img src=\"../../../../assets/img/producto.png\" width=\"100%\" alt=\"\" /></a>\n      <a class=\"title_card\" href=\"/#/product\"> <h3>PRODUCTO</h3> </a>\n      <a class=\"text_card\" href=\"/#/product\"> <p>Te ayudamos a mejorar tu línea de productos con soluciones innovadoras y atractivas en los mercados internacionales. Te acompañamos hasta la producción piloto de tu producto.</p> </a>\n      <a class=\"see_more\" href=\"/#/product\"> <div>+</div> </a>\n    </div>\n    <div class=\"col-md-4 card_neura\">\n      <a  href=\"/#/tech\">\n        <img class=\"img_card_ser\" src=\"../../../../assets/img/software.png\" width=\"100%\" alt=\"\" /> </a>\n      <a class=\"title_card\" href=\"/#/tech\"><h3>TECNOLOGÍA</h3> </a>\n      <a class=\"text_card\" href=\"/#/tech\"><p>Te ayudamos a crear herramientas digitales poderosas capaces de realizar las tareas másexigentes que puedas imaginar. Entregamos productos completos y funcionales, así como mantenimiento de los mismos.</p> </a>\n      <a class=\"see_more\" href=\"/#/tech\"> <div>+</div> </a>\n    </div>\n\n  </div>\n<br><br>\n\n<!-- STORIES -->\n  <div class=\"row\">\n    <div class=\"col-md-8\">\n      <p>Nos encanta participar en proyectos interesantes y ambisiosos aquí puedes encontrar los últimos proyectos en los que hemos tenido el placer de participar.</p>\n    </div>\n\n  </div>\n\n  <div class=\"row\">\n    <div class=\"col-md-4 card_neura\"> <a class=\"img_card\" href=\"#\">\n      <img src=\"../../../../assets/img/stories/temika.png\" width=\"100%\" alt=\"TEMIKA CONSULT\" /></a>\n      <a class=\"title_card\" href=\"/#/\"><h4><br>TEMIKA CONSULT</h4></a>\n      <a class=\"text_card\" href=\"/#/\"><p>Branding de firma de\nlicenciados para\nemprendedores.</p></a>\n    </div>\n    <div class=\"col-md-4 card_neura\"> <a class=\"img_card\" href=\"#\">\n      <img src=\"../../../../assets/img/stories/sillasonora.png\" width=\"100%\" alt=\"SILLA SONORA\" /></a>\n      <a class=\"title_card\" href=\"/#/\"><h4><br>SILLA SONORA</h4></a>\n      <a class=\"text_card\" href=\"/#/\"><p>Diseño, desarrollo y prototipado\nde Silla para concurso de diseño\nindustrial.</p></a>\n    </div>\n    <div class=\"col-md-4 card_neura\"> <a class=\"img_card\" href=\"#\">\n      <img src=\"../../../../assets/img/stories/lakshmi.png\" class=\"img-fluid pull-xs-left\" alt=\"LAKSHMI\"></a>\n      <a class=\"title_card\" href=\"/#/\"><h4><br>LAKSHMI</h4></a>\n      <a class=\"text_card\" href=\"/#/\"><p>Branding para marca de bolsas\nde piel hechas en México.</p></a>\n    </div>\n  </div>\n  <div class=\"col-md-3 offset-md-9\">\n    <a class=\"link_neuracore_style\" href=\"/#/stories\">STORIES</a>\n  </div>\n\n</div>\n\n<br/><br/>\n\n<!-- NUESTROS CLIENTES -->\n<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"col-md-8\">\n      <h2 class=\"title_neura\">NUESTROS CLIENTES</h2>\n    <p>  Nuestros clientes hablan por nosotros y siempre es un placer poder participar en el desarrollo\nde sus empresas y sus proyectos.</p>\n    </div>\n\n  </div>\n<br><br>\n</div>\n\n<!-- CLIENTES -->\n\n<div class=\" container\">\n\n\n<div class=\"row justify-content-center\">\n<!-- la clase cleint permite el hover de girs-color -->\n  <div class=\"client col-md-3 col-sm-4\">\n    <img src=\"../../../../assets/vectors/l_copival_color.svg\"  alt=\"\">\n  </div>\n  <div class=\"client col-md-3 col-sm-4\">\n    <img src=\"../../../../assets/vectors/l_clemde_color.svg\"  alt=\"\">\n  </div>\n  <div class=\"client col-md-3 col-sm-4\">\n    <img  src=\"../../../../assets/vectors/l_bitlegends_color.svg\"  alt=\"\">\n  </div>\n  <div class=\"client col-md-3 col-sm-4\">\n    <img src=\"../../../../assets/vectors/l_sahlo_color.svg\"  alt=\"\">\n  </div>\n  <div class=\"client col-md-3 col-sm-4\">\n      <img src=\"../../../../assets/vectors/l_temikaconsult_color.svg\"  alt=\"\">\n  </div>\n\n\n  <div class=\"client col-md-3 col-sm-4\">\n    <img src=\"../../../../assets/vectors/l_lakshmi_color.svg\"  alt=\"\">\n  </div>\n  <div class=\"client col-md-3 col-sm-4\">\n    <img src=\"../../../../assets/vectors/l_arnic_color.svg\"  alt=\"\">\n  </div>\n  <div class=\"client col-md-3 col-sm-4\">\n    <img src=\"../../../../assets/vectors/l_tenorioconstruccion_color.svg\"  alt=\"\">\n  </div>\n  <div class=\"client col-md-3 col-sm-4\">\n    <img src=\"../../../../assets/vectors/l_metalplex_color.svg\"  alt=\"\">\n  </div>\n  <div class=\"client col-md-3 col-sm-4\">\n    <img src=\"../../../../assets/vectors/l_mm_color.svg\"  alt=\"\">\n  </div>\n\n  </div>\n  </div><br>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-home',
            template: __webpack_require__("../../../../../src/app/components/home/home.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "<header id=\"header\">\n\n<div class=\"\">\n\n\n<nav class=\"navbar navbar-expand-lg \">\n    <a class=\"navbar-brand\" href=\"#\"><img src=\"/assets/logo_neuracore.svg\" class=\"img_nav_neura\" alt=\"\"></a>\n\n    <a class=\"navbar-toggler\"  data-toggle=\"collapse\" data-target=\"#navbarTogglerDemo02\" aria-controls=\"navbarTogglerDemo02\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n        <img class=\"navbar-toggler-icon\" src=\"/assets/menu-button.svg\" height=\"40\" alt=\"\">\n    </a>\n\n    <div class=\"collapse navbar-collapse nav_neura\" id=\"navbarTogglerDemo02\">\n\n        <ul class=\"navbar-nav mt-2 ml-auto mt-lg-0\">\n            <li class=\"nav-item\" routerLinkActive=\"active\">\n                <span data-toggle=\"collapse\" data-target=\"#navbarTogglerDemo02\">\n                <a class=\"nav-link nav_li\" [routerLink]=\"['home']\">\n\n                    INICIO\n\n                </a>\n                </span>\n            </li>\n            <li class=\"nav-item\" routerLinkActive=\"active\">\n              <span data-toggle=\"collapse\" data-target=\"#navbarTogglerDemo02\">\n                <a class=\"nav-link nav_li\" href=\"/#nosotros_neura\">\n\n                    NOSOTROS\n\n                </a>\n                </span>\n            </li>\n\n          <!-- Queda pendiente el enrutar los servosios se hara desde el home por ahora -->\n          <!-- <li class=\"nav-item\" routerLinkActive=\"active\">\n            <a class=\"nav-link\" [routerLink]=\"['services']\">SERVICIOS</a>\n          </li> -->\n\n            <li class=\"nav-item dropdown\">\n                <a class=\"nav-link dropdown-toggle nav_li\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                    SERVICIOS\n                </a>\n                <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n                  <span data-toggle=\"collapse\" data-target=\"#navbarTogglerDemo02\">\n                    <a class=\"dropdown-item\" href=\"/#/branding/#portadita\">\n\n                        BRANDING\n\n                    </a>\n                    </span>\n                    <span data-toggle=\"collapse\" data-target=\"#navbarTogglerDemo02\">\n                    <a class=\"dropdown-item\" [routerLink]=\"['product']\">\n\n                        PRODUCTO\n\n                    </a>\n                    </span>\n                    <span data-toggle=\"collapse\" data-target=\"#navbarTogglerDemo02\">\n                    <a class=\"dropdown-item\" [routerLink]=\"['tech']\">\n\n                        TECNOLOGÍA\n\n                    </a>\n                    </span>\n                </div>\n            </li>\n\n            <li class=\"nav-item\" routerLinkActive=\"active\">\n              <span data-toggle=\"collapse\" data-target=\"#navbarTogglerDemo02\">\n                <a class=\"nav-link nav_li\" [routerLink]=\"['stories']\">\n\n                    STORIES\n\n                </a>\n                </span>\n            </li>\n            <li class=\"nav-item\" routerLinkActive=\"active\">\n              <span data-toggle=\"collapse\" data-target=\"#navbarTogglerDemo02\">\n                <a class=\"nav-link nav_li\" [routerLink]=\"['talks']\">\n\n                    TALKS\n\n                </a>\n                </span>\n            </li>\n              <!-- el contacto aun no esta enrutado ya que es parte de footer y esta en todas las páginas -->\n            <li class=\"nav-item\" routerLinkActive=\"active\">\n              <span data-toggle=\"collapse\" data-target=\"#navbarTogglerDemo02\">\n                <a class=\"nav-link nav_li\" [routerLink]=\"['contact']\" >\n                  CONTACTO\n                </a>\n              </span>\n            </li>\n            <!-- <li class=\"nav-item\"> <a class=\"nav-link\" href=\"#\">EN | ES</a> </li> -->\n        </ul>\n      </div>\n</nav>\n</div>\n\n\n\n\n<!-- menu movil -->\n\n\n\n\n\n<!-- <nav class=\"navbar-toggleable-md navbar-light fixed-top\">\n  <button class=\"my-3 navbar-toggler navbar-toggler-right custom-toggler \" data-target=\"#navbarContent\" data-toggle=\"collapse\" type=\"button\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button>\n  <a class=\"navbar-brand\" href=\"index.html\">\n    <img alt=\"MAKING THINGS HAPPENS\" height=\"80\" src=\"/assets/logo_neuracore.svg\" />\n  </a>\n<div class=\"collapse navbar-collapse\" id=\"navbarContent\">\n<ul class=\"navbar-nav ml-auto\">\n\t<li class=\"nav-item mx-3\">\n    <a class=\"button_neuracore_style button_menu nav-link\" data-target=\"#services-modal\" data-toggle=\"modal\" id=\"button-services\"> SERVICIOS </a>\n    <a class=\"nav-link dropdown-toggle nav_li\" href=\"#\" id=\"navbarDropdownMenuLink\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n        SERVICIOS\n    </a>\n    <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdownMenuLink\">\n        <a class=\"dropdown-item\" href=\"/#/branding/#portadita\">BRANDING</a>\n        <a class=\"dropdown-item\" [routerLink]=\"['product']\">PRODUCTO</a>\n        <a class=\"dropdown-item\" [routerLink]=\"['tech']\">TECNOLOGÍA</a>\n    </div>\n  </li>\n\t<li class=\"nav-item mx-3\">\n    <a class=\"ancla button_neuracore_style button_menu nav-link\" data-scroll=\"\" href=\"#nosotros\">NOSOTROS </a>\n  </li>\n\t<li class=\"nav-item mx-3\">\n    <a class=\"ancla button_neuracore_style button_menu nav-link\" data-scroll=\"\" href=\"#portafolio\">PORTAFOLIO </a>\n  </li>\n\t<li class=\"nav-item mx-3\">\n    <a class=\"button_neuracore_style button_menu nav-link\" data-scroll=\"\" href=\"/#conta_neura\"> <span>CONTACTO</span> </a>\n  </li>\n\t<li class=\"nav-item mx-3\">\n    <a class=\"button_neuracore_style button_menu nav-link\" href=\"https://blog.neuracorestudio.com/\"> <span>BLOG</span> </a>\n  </li>\n\n</ul>\n</div>\n</nav> -->\n\n\n\n</header>\n"

/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NavbarComponent = /** @class */ (function () {
    function NavbarComponent() {
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__("../../../../../src/app/components/navbar/navbar.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/privacy/privacy.component.html":
/***/ (function(module, exports) {

module.exports = "<body ondragstart=\"return false\" onselectstart=\"return false\" oncontextmenu=\"return false\">\n\n\n\t<div class=\"animated fadeIn container\">\n    <br><br><br><br><br><br>\n<h2>AVISO DE PRIVACIDAD </h2>\n\t\t<p>\n\t\t\tROMEO ISAÍ RAMOS HERNANDEZ, mejor conocido como NEURACORE STUDIO, con domicilio en calle CAMPECHE 233, colonia CONDESA, CIUDAD DE MÉXICO, municipio o delegación CUAUHTÉMOC, c.p. 06100, país México, y portal de internet www.neuracorestudio.com, es el responsable del uso y protección de sus datos personales, y al respecto le informamos lo siguiente: <br/><br/>\n\n\t\t\t¿Para qué fines utilizaremos sus datos personales? <br/>\n\n\t\t\tLos datos personales que recabamos de usted, los utilizaremos para las siguientes finalidades que son necesarias para el servicio que solicita: <br/><br/>\n\t\t\tPara generar documentos en servicios recurrentes tales como contratos, facturas, convenios, etc.<br/><br/>\n\t\t\tPara administrar y consultar los antecedentes profesionales en los procesos de reclutamiento de la empresa.<br/><br/>\n\t\t\tDe manera adicional, utilizaremos su información personal para las siguientes finalidades secundarias que no son necesarias para el servicio solicitado, pero que nos permiten y facilitan brindarle una mejor atención:<br/><br/>\n\t\t\tPara administrar y operar los servicios que usted solicita con nosotros.<br/><br/>\n\t\t\tPara verificar su identidad y confirmar que es una persona real.<br/><br/>\n\t\t\tMercadotecnia o publicitaria<br/><br/>\n\t\t\tProspección comercial<br/><br/>\n\n\n\nEn caso de que no desee que sus datos personales sean tratados para estos fines secundarios, desde este momento usted nos puede comunicar lo anterior a través del siguiente mecanismo:<br/>\n\nCorreo electrónico<br/>\n\nLa negativa para el uso de sus datos personales para estas finalidades no podrá ser un motivo para que le neguemos los servicios y productos que solicita o contrata con nosotros.<br/>\n\n¿Qué datos personales utilizaremos para estos fines? <br/>\n\nPara llevar a cabo las finalidades descritas en el presente aviso de privacidad, utilizaremos los siguientes datos personales:<br/><br/>\n•\tNombre<br/>\n•\tEstado Civil<br/>\n•\tRegistro Federal de Contribuyentes(RFC)<br/>\n•\tClave única de Registro de Población (CURP)<br/>\n•\tLugar de nacimiento<br/>\n•\tFecha de nacimiento<br/>\n•\tNacionalidad<br/>\n•\tDomicilio<br/>\n•\tTeléfono particular<br/>\n•\tTeléfono celular<br/>\n•\tCorreo electrónico<br/>\n•\tEdad<br/>\n•\tFotografía<br/>\n•\tEstatura<br/>\n•\tPeso<br/>\n•\tTipo de sangre<br/>\n•\tPuesto o cargo que desempeña<br/>\n•\tDomicilio de trabajo<br/>\n•\tCorreo electrónico institucional<br/>\n•\tReferencias laborales<br/>\n•\tInformación generada durante los procesos de reclutamiento, selección y contratación<br/>\n•\tCapacitación laboral<br/>\n•\tTrayectoria educativa<br/>\n•\tTítulos<br/>\n•\tCédula profesional<br/>\n•\tCertificados<br/>\n•\tReconocimientos<br/>\n•\tInformación fiscal<br/>\n•\tCuentas bancarias<br/><br/>\n\n¿Cómo puede acceder, rectificar o cancelar sus datos personales, u oponerse a su uso? <br/>\n\nUsted tiene derecho a conocer qué datos personales tenemos de usted, para qué los utilizamos y las condiciones del uso que les damos (Acceso). Asimismo, es su derecho solicitar la corrección de su información personal en caso de que esté desactualizada, sea inexacta o incompleta (Rectificación); que la eliminemos de nuestros registros o bases de datos cuando considere que la misma no está siendo utilizada adecuadamente (Cancelación); así como oponerse al uso de sus datos personales para fines específicos (Oposición). Estos derechos se conocen como derechos ARCO.<br/>\n\nPara el ejercicio de cualquiera de los derechos ARCO, usted deberá presentar la solicitud respectiva a través del siguiente medio: <br/>\n\nEnviando un correo electrónico a legal@neuracore.mx <br/>\n\nCon relación al procedimiento y requisitos para el ejercicio de sus derechos ARCO, le informamos lo siguiente:<br/><br/>\na) ¿A través de qué medios pueden acreditar su identidad el titular y, en su caso, su representante, así como la personalidad este último?<br/>\nA través de documentación oficial.<br/><br/>\n\nb) ¿Qué información y/o documentación deberá contener la solicitud?<br/>\nComprobante de domicilio, identificación oficial, etc.<br/><br/>\n\nc) ¿En cuántos días le daremos respuesta a su solicitud?<br/>\n5 días hábiles<br/><br/>\n\nd) ¿Por qué medio le comunicaremos la respuesta a su solicitud?<br/>\npor medio de correo electrónico<br/><br/>\n\ne) ¿En qué medios se pueden reproducir los datos personales que, en su caso, solicite?<br/>\ninternet<br/><br/>\n\ng) Para mayor información sobre el procedimiento, ponemos a disposición los siguientes medios:<br/>\n015556844452<br/><br/>\n\n\nLos datos de contacto de la persona o departamento de datos personales, que está a cargo de dar trámite a las solicitudes de derechos ARCO, son los siguientes: <br/><br/>\n\na) Nombre de la persona o departamento de datos personales: ELEAZAR DANIEL SAÚZ RANGEL<br/>\nb) Domicilio: calle PIRÁMIDE DE LA LUNA, AVANTE, colonia AVANTE, ciudad COYOACÁN, municipio o delegación COYOACÁN, c.p. 04460, en la entidad de CIUDAD DE MEXICO, país México<br/>\nc) Correo electrónico: e.sauz@neuracore.mx<br/><br/>\n\nUsted puede revocar su consentimiento para el uso de sus datos personales<br/>\n\nUsted puede revocar el consentimiento que, en su caso, nos haya otorgado para el tratamiento de sus datos personales. Sin embargo, es importante que tenga en cuenta que no en todos los casos podremos atender su solicitud o concluir el uso de forma inmediata, ya que es posible que por alguna obligación legal requiramos seguir tratando sus datos personales. Asimismo, usted deberá considerar que para ciertos fines, la revocación de su consentimiento implicará que no le podamos seguir prestando el servicio que nos solicitó, o la conclusión de su relación con nosotros.<br/>\n\nPara revocar su consentimiento deberá presentar su solicitud a través del siguiente medio: <br/>\n\nvía correo electrónico <br/>\n\nCon relación al procedimiento y requisitos para la revocación de su consentimiento, le informamos lo siguiente: <br/><br/>\n\na) ¿A través de qué medios pueden acreditar su identidad el titular y, en su caso, su representante, así como la personalidad este último?<br/>\na través de documentación oficial<br/><br/>\n\nb) ¿Qué información y/o documentación deberá contener la solicitud?<br/>\ncomprobante de domicilio e identificación oficial<br/><br/>\n\nc) ¿En cuántos días le daremos respuesta a su solicitud?<br/>\n5 a 7 días hábiles<br/><br/>\n\nd) ¿Por qué medio le comunicaremos la respuesta a su solicitud?<br/>\nvía correo electrónico<br/><br/>\n\n¿Cómo puede limitar el uso o divulgación de su información personal? <br/>\n\nCon objeto de que usted pueda limitar el uso y divulgación de su información personal, le ofrecemos los siguientes medios: <br/>\n\nSolicitud vía correo electrónico<br/>\n\nAsimismo, usted se podrá inscribir a los siguientes registros, en caso de que no desee obtener publicidad de nuestra parte: <br/>\n\nRegistro Público para Evitar Publicidad, para mayor información consulte el portal de internet de la PROFECO<br/>\nRegistro Público de Usuarios, para mayor información consulte el portal de internet de la CONDUSEF\nEl uso de tecnologías de rastreo en nuestro portal de internet<br/>\n\nLe informamos que en nuestra página de internet utilizamos cookies, web beacons u otras tecnologías, a través de las cuales es posible monitorear su comportamiento como usuario de internet, así como brindarle un mejor servicio y experiencia al navegar en nuestra página. Los datos personales que recabamos a través de estas tecnologías, los utilizaremos para los siguientes fines:<br/>\n\nPara optimizar la usabilidad del sitio web<br/>\n\nLos datos personales que obtenemos de estas tecnologías de rastreo son los siguientes:<br/><br/>\n\nIdioma preferido por el usuario<br/>\nRegión en la que se encuentra el usuario<br/>\nTipo de navegador del usuario<br/>\nTipo de sistema operativo del usuario<br/>\n\n\n¿Cómo puede conocer los cambios en este aviso de privacidad?<br/>\n\nEl presente aviso de privacidad puede sufrir modificaciones, cambios o actualizaciones derivadas de nuevos requerimientos legales; de nuestras propias necesidades por los productos o servicios que ofrecemos; de nuestras prácticas de privacidad; de cambios en nuestro modelo de negocio, o por otras causas.<br/>\n\nNos comprometemos a mantenerlo informado sobre los cambios que pueda sufrir el presente aviso de privacidad, a través de: vía correo electrónico.<br/>\n\nEl procedimiento a través del cual se llevarán a cabo las notificaciones sobre cambios o actualizaciones al presente aviso de privacidad es el siguiente: <br/>\n\nSe enviará un correo electrónico con las actualizaciones de las condiciones y uso de datos.\n\t\t</p>\n\t\t<br/><br/><br/><br/>\n</div>\n\n</body>\n"

/***/ }),

/***/ "../../../../../src/app/components/privacy/privacy.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrivacyComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PrivacyComponent = /** @class */ (function () {
    function PrivacyComponent() {
    }
    PrivacyComponent.prototype.ngOnInit = function () {
    };
    PrivacyComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-privacy',
            template: __webpack_require__("../../../../../src/app/components/privacy/privacy.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], PrivacyComponent);
    return PrivacyComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/services/branding/branding.component.html":
/***/ (function(module, exports) {

module.exports = "\n<div  class=\"animated fadeIn\">\n<div class=\"container \">\n\n<div id=\"portadita\" class=\"portada portada_neura cont_item row \">\n  <div class=\"col-lg-5 offset-md-1 \">\n\n    <h2 class=\"mth\">BRANDING</h2>\n  </div>\n  <div class=\"col-lg-6\">\n    <p>\n      Las empresas tienen una historia que contar y debe ser\nidentificada inmediatamente a través del mensaje que\nnuestra <span class=\"red\">identidad corporativa</span>, logo, eslogan y publicidad\ntransmiten a nuestros consumidores, competidores\ny mercado en general.\n    </p>\n\n  </div>\n</div>\n\n<br><br><br><br>\n\n<div class=\"row\">\n  <div class=\"col-md-5\">\n    <p>Te ayudamos a diseñar y desarrollar la identidad de tu\nmarca y todos sus elementos como lo son: su <span class=\"red\">nombre</span>,\n<span class=\"red\">logotipo</span>, <span class=\"red\">colores</span>, <span class=\"red\">aplicaciones de marca</span>, <span class=\"red\">uso\ncorrecto del arte e identidad visual</span>, <span class=\"red\">personalidad</span>\ny <span class=\"red\">lenguaje</span> de forma que todo sea coherente y\ncomprensible para tus clientes potenciales.</p>\n  </div>\n  <div class=\"col-md-6\">\n\n  </div>\n</div>\n\n</div>\n\n<!-- End Portada -->\n\n<!-- Banner sin container -->\n\n<img src=\"../../../../assets/img/banner_branding.png\" width=\"100%\" alt=\"...\">\n<br><br><br><br><br><br>\n\n<div class=\"container\">\n<!-- METODOLOGÍA -->\n  <div class=\"row justify-content-end\">\n\n    <div class=\"col-md-6\">\n      <p>Sabemos que <span class=\"red\">los propósitos de la marca</span> y de la empresa variarán dependiendo\nde distintos factores y para lograr la construcción más adecuada de cualquier tipo de\nmarca por lo que nuestra metodología nos permite empezar desde cualquier etapa y, si\nes necesario, regresar a cualquier punto anterior para mejorar los resultados de tu\nproyecto. que nos permite llegar de una idea a un proyecto final viable.</p>\n    </div>\n  </div>\n\n  <div class=\"row\">\n    <div class=\"col-md-6\">\n      <!-- la clase pull-xs-left no se que sea la neta, se puso sola y la deje :) -->\n      <img src=\"../../../../assets/vectors/metodo_branding.svg\" class=\"img-fluid pull-xs-left\" alt=\"...\">\n    </div>\n    <div class=\"col-md-6\">\n\n    </div>\n    </div>\n\n    <div class=\"row justify-content-end\">\n      <div class=\"col-md-6\">\n        <p>En todo el proceso de desarrollo del servicio\n<span class=\"red\">buscamos involucrarte</span>, realizamos una\nserie de sesiones para determinar y asimilar\nla visión, exigencias y necesidades que\ntienes mediante una serie de actividades y\nestrategias focalizadas para tal propósito.</p>\n      </div>\n\n  </div>\n\n</div>\n<br><br><br><br>\n\n<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"col-md-8\">\n      <p>En <span class=\"neura\">NEURA</span><span class=\"core\">CORE</span> <span class=\"studio\">STUDIO</span> no diseñamos logos, <br><span class=\"red\">construimos marcas.</span></p>\n    </div>\n\n  </div>\n  <div class=\"col-md-3 offset-md-9\">\n    <a class=\"link_neuracore_style\" href=\"/#/product\">SIGUIENTE</a>\n  </div>\n  <br><br><br><br>\n  <div class=\"row\">\n\n    <div class=\"col-md-4\"> <a class=\"img_card\" href=\"#\">\n      <img src=\"../../../../assets/img/stories/temika.png\" width=\"100%\" alt=\"TEMIKA CONSULT\" /></a>\n        <a class=\"title_card\" href=\"/#/\"><h4><br>TEMIKA CONSULT</h4></a>\n      <a class=\"text_card\" href=\"/#/\"><p>Branding de firma de licenciados para emprendedores.</p></a>\n    </div>\n\n    <div class=\"col-md-4\"> <a class=\"img_card\" href=\"#\">\n      <img src=\"../../../../assets/img/stories/metalplex.png\" width=\"100%\" alt=\"METALPEX\" /></a>\n      <a class=\"title_card\" href=\"/#/\"><h4><br>METALPEX</h4></a>\n      <a class=\"text_card\" href=\"/#/\"><p>Branding para empresa de maquila metalmecánica</p></a>\n    </div>\n\n    <div class=\"col-md-4\"> <a class=\"img_card\" href=\"#\">\n      <img src=\"../../../../assets/img/stories/lakshmi.png\" class=\"img-fluid pull-xs-left\" alt=\"LAKSHMI\"></a>\n      <a class=\"title_card\" href=\"/#/\"><h4><br>LAKSHMI</h4></a>\n      <a class=\"text_card\" href=\"/#/\"><p>Branding para marca de bolsas de piel hechas en México.</p></a>\n    </div>\n  </div>\n  <div class=\"col-md-3 offset-md-9\">\n    <a class=\"link_neuracore_style\" href=\"/#/stories\">STORIES</a>\n  </div>\n\n</div>\n<br><br><br><br>\n\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/services/branding/branding.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BrandingComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BrandingComponent = /** @class */ (function () {
    function BrandingComponent() {
    }
    BrandingComponent.prototype.ngOnInit = function () {
    };
    BrandingComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-branding',
            template: __webpack_require__("../../../../../src/app/components/services/branding/branding.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], BrandingComponent);
    return BrandingComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/services/product/product.component.html":
/***/ (function(module, exports) {

module.exports = "<div  class=\"animated fadeIn\">\n\n<div class=\"container\">\n\n<div class=\"portada portada_neura cont_item row\">\n  <div class=\"col-lg-5 offset-md-1\">\n    <h2 class=\"mth\">PRODUCTO</h2>\n  </div>\n  <div class=\"col-lg-6\">\n    <p> <br>\n      Te ayudamos a culminar ese nuevo proyecto que servirá para ampliar tu catálogo y te ayudará a impactar más clientes de forma positiva, incrementando tu visibilidad y participación de mercado por la inventiva, estética y calidad de tus productos.\n    </p>\n\n    <!-- <p>\n      Bienvenido a <span class=\"neura\">NEURA</span><span class=\"core\">CORE</span> <span class=\"studio\">STUDIO</span> el lugar en donde tus ideas alcanzan todo su potencial.\n    </p> -->\n  </div>\n\n</div>\n\n<br><br><br><br>\n\n<div class=\"row\">\n  <div class=\"col-md-5\">\n    <p>Creamos productos desde cero, desde la <span class=\"red\">investigación de mercado</span>\n para identificar necesidades, <span class=\"red\">hasta la\n  supervisión de tu primera producción.</span> Para logar-\n  lo seguimos un proceso en el que siempre estarás invo-\n  lucrado con la finalidad de mantenerte siempre informado\n  sobre el estatus y avances de tu proyecto.</p>\n  </div>\n  <div class=\"col-md-6\">\n\n  </div>\n</div>\n<br><br><br>\n\n</div>\n<!-- End Portada -->\n\n<!-- Banner sin container -->\n\n<img src=\"../../../../assets/img/banner_s_producto.png\" width=\"100%\" alt=\"...\">\n<br><br><br><br><br><br>\n\n<div class=\"container\">\n<!-- METODOLOGÍA -->\n  <div class=\"row justify-content-end\">\n\n    <div class=\"col-md-6\">\n      <p><span class=\"red\">Nuestro proceso y metodología</span> nos\npermite realizar cualquier tipo de producto y terminar\ntus proyectos en tiempos muy competitivos, así\ncomo realizar todas las etapas de este por separado,\nde forma que si lo que requieres es el rediseño de\nun producto existente, o sólo la información de\nproducción, podemos realizarlo sin ningún problema.\n</p>\n    </div>\n  </div>\n\n  <div class=\"row\">\n    <div class=\"col-md-6\">\n      <!-- la clase pull-xs-left no se que sea la neta, se puso sola y la deje :) -->\n      <img src=\"../../../../assets/vectors/metodo_producto.svg\" class=\"img-fluid pull-xs-left\" alt=\"...\">\n    </div>\n    <div class=\"col-md-6\">\n\n    </div>\n    </div>\n\n    <div class=\"row justify-content-end\">\n      <div class=\"col-md-6\">\n        <p>Todo nuestro proceso es cíclico y está estructurado en etapas que pueden realizarse de manera individual, lo que significa que podemos iniciar en cualquiera de ellas y regresarnos en caso de necesidades particulæs de tu proyecto.</p>\n      </div>\n\n  </div>\n\n</div>\n\n<br><br><br><br>\n\n<!-- STORIES -->\n<div class=\"container\">\n\n  <div class=\"row\">\n    <div class=\"col-md-8\">\n      <p>En <span class=\"neura\">NEURA</span><span class=\"core\">CORE</span> <span class=\"studio\">STUDIO</span> no diseñamos productos,<br><span class=\"red\">materializamos ideas.</span> </p>\n    </div>\n\n  </div>\n  <div class=\"col-md-3 offset-md-9\">\n    <a class=\"link_neuracore_style\" href=\"/#/tech\">SIGUIENTE</a>\n  </div>\n  <br><br><br><br>\n\n  <div class=\"row\">\n    <div class=\"col-md-4\"> <a class=\"img_card\" href=\"#\">\n      <img src=\"../../../../assets/img/stories/spiglocase.png\" width=\"100%\" alt=\"SPIGLOCASE\" /></a>\n        <a class=\"title_card\" href=\"/#/\"><h4><br>SPIGLOCASE</h4></a>\n      <a class=\"text_card\" href=\"/#/\"><p>Diseño y desarrollo de accesorio para celulares, próximamente a la venta.</p></a>\n    </div>\n    <div class=\"col-md-4\"> <a class=\"img_card\" href=\"#\">\n      <img src=\"../../../../assets/img/stories/sillasonora.png\" width=\"100%\" alt=\"SILLA SONORA\" /></a>\n      <a class=\"title_card\" href=\"/#/\"><h4><br>SILLA SONORA</h4></a>\n      <a class=\"text_card\" href=\"/#/\"><p>Diseño, desarrollo y prototipado\n    de Silla para concurso de diseño\n    industrial.</p></a>\n    </div>\n    <div class=\"col-md-4\"> <a class=\"img_card\" href=\"#\">\n      <img src=\"../../../../assets/img/stories/mushroombowl.png\" class=\"img-fluid pull-xs-left\" alt=\"MUSRHOOM BOWL\"></a>\n      <a class=\"title_card\" href=\"/#/\"><h4><br>MUSRHOOM BOWL</h4></a>\n      <a class=\"text_card\" href=\"/#/\"><p>Diseño y desarrollo hasta producción piloto de utensilios de cocina para Bit Legends.</p></a>\n    </div>\n  </div>\n  <div class=\"col-md-3 offset-md-9\">\n    <a class=\"link_neuracore_style\" href=\"/#/stories\">STORIES</a>\n  </div>\n\n</div>\n<br><br><br><br>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/services/product/product.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProductComponent = /** @class */ (function () {
    function ProductComponent() {
    }
    ProductComponent.prototype.ngOnInit = function () {
    };
    ProductComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-product',
            template: __webpack_require__("../../../../../src/app/components/services/product/product.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], ProductComponent);
    return ProductComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/services/services.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  services works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/components/services/services.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServicesComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ServicesComponent = /** @class */ (function () {
    function ServicesComponent() {
    }
    ServicesComponent.prototype.ngOnInit = function () {
    };
    ServicesComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-services',
            template: __webpack_require__("../../../../../src/app/components/services/services.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], ServicesComponent);
    return ServicesComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/services/tech/tech.component.html":
/***/ (function(module, exports) {

module.exports = "<div  class=\"animated fadeIn\">\n<div class=\"container\">\n\n<div class=\"portada portada_neura cont_item row \">\n  <div class=\"col-lg-5 offset-md-1 \">\n\n<!-- el tamaño de fuente de este h2 lo puse asi aqui porque es una muy gande y pegaba con el parrafo -->\n    <h2 class=\"mth\">TECNOLOGÍA</h2>\n  </div>\n  <div class=\"col-lg-6\">\n    <p>\n      La empresas cuentan con distintas operaciones que\npodrían ser automatizadas, facilitadas o posibles por\nmedio de la tecnología y así mantenerse competitivas\nen su sector.\n    </p>\n    \n    <!-- <p>\n      Bienvenido a  el lugar en donde tus ideas alcanzan todo su potencial.\n    </p> -->\n  </div>\n</div>\n\n<br><br><br><br>\n\n<div class=\"row\">\n  <div class=\"col-md-5\">\n    <p>Te ayudamos a implementar <span class=\"red\">novedosos sistemas</span> tecnológicos en las distintas áreas de tu empresa, desde <span class=\"red\">sistemas existentes</span>, hasta el <span class=\"red\">diseño y desarrollo de tecnología a la medida</span> de tus necesidades.</p>\n  </div>\n  <div class=\"col-md-6\">\n\n  </div>\n</div>\n\n</div>\n<!-- End Portada -->\n\n<!-- Banner sin container -->\n\n<!-- <div class=\"jarallax\">\n<img class=\"jarallax-img\" src=\"../../../../assets/img/banner_tech.png\" width=\"100%\" alt=\"...\"></div> -->\n\n<div style= \"background: url(../../../../assets/img/banner_tech.png) no-repeat fixed 0px 140px;\n  width: 100%; height: 100%;\n  padding-top: 120px;\n  padding-bottom: 120px;\n  background-size:100%;\"\n  class=\"baner_neura\">\n\n</div>\n<br><br><br><br><br><br>\n\n\n<div class=\"container\">\n<!-- METODOLOGÍA -->\n  <div class=\"row justify-content-end\">\n\n    <div class=\"col-md-6\">\n      <p> <span class=\"red\">Nuestro proceso y metodología</span> nos permite cumplir con las expectativas y alcances planteados de un inicio, comprometiéndonos con la <span class=\"red\">calidad necesaria</span>\n para cumplir de la mejor forma posible con los requerimientos y funcionalidades más exigentes.</p>\n    </div>\n  </div>\n\n  <div class=\"row\">\n    <div class=\"col-md-6\">\n      <!-- la clase pull-xs-left no se que sea la neta, se puso sola y la deje :) -->\n      <img src=\"../../../../assets/vectors/metodo_tech.svg\" class=\"img-fluid pull-xs-left\" alt=\"...\">\n    </div>\n    <div class=\"col-md-6\">\n\n    </div>\n    </div>\n\n    <div class=\"row justify-content-end\">\n      <div class=\"col-md-6\">\n        <p>Conocemos la importancia indiscutible del papel que juega la tecnología en los\nnegocios, desde <span class=\"red\">tener presencia</span> en la red con una <span class=\"red\">página web</span> hasta mantener las <span class=\"red\">operaciones</span> de la empresa de <span class=\"red\">forma remota</span>, teniendo accesibilidad al trabajo desde cualquier parte del mundo y en todo momento, pasando por <span class=\"red\">simplificar y automatizar</span> procesos que pueden ser realizados mejor forma con <span class=\"red\">soluciones digitales</span>.</p>\n      </div>\n\n  </div>\n\n</div>\n<br><br><br><br>\n<!-- STORIES -->\n<div class=\"container\">\n  <div class=\"row\">\n    <div class=\"col-md-8\">\n      <p>En <span class=\"neura\">NEURA</span><span class=\"core\">CORE</span> <span class=\"studio\">STUDIO</span> no desarrollamos tecnología,<br><span class=\"red\">llevamos negocios al siglo XXI.</span></p>\n    </div>\n\n  </div>\n<div class=\"col-md-3 offset-md-9\">\n  <a class=\"link_neuracore_style\" href=\"/#/branding\">SIGUIENTE</a>\n</div>\n<br><br><br><br>\n  <div class=\"row\">\n\n\n    <div class=\"col-md-4\"> <a class=\"img_card\" href=\"#\">\n      <img src=\"../../../../assets/img/stories/arnic.png\" class=\"img-fluid pull-xs-left\" alt=\"ARINC\"></a>\n      <a class=\"title_card\" href=\"/#/\"><h4><br>ARINC</h4></a>\n      <a class=\"text_card\" href=\"/#/\"><p>Desarrollo de landing page responsiva, para empresa de servicios profesionales.</p></a>\n    </div>\n\n\n    <div class=\"col-md-4\"> <a class=\"img_card\" href=\"#\">\n      <img src=\"../../../../assets/img/stories/velvet.png\" class=\"img-fluid pull-xs-left\" alt=\"VELVET\"></a>\n      <a class=\"title_card\" href=\"/#/\"><h4><br>VELVET</h4></a>\n      <a class=\"text_card\" href=\"/#/\"><p>Desarrollo de la primera plataforma de interacción comercial en México.</p></a>\n    </div>\n\n    <div class=\"col-md-4\"> <a class=\"img_card\" href=\"#\">\n      <img src=\"../../../../assets/img/stories/virtef.png\" class=\"img-fluid pull-xs-left\" alt=\"VIRTEF\"></a>\n      <a class=\"title_card\" href=\"/#/\"><h4><br>VIRTEF</h4></a>\n      <a class=\"text_card\" href=\"/#/\"><p>Diseño de landing page para plataforma de finanzas personales.</p></a>\n    </div>\n\n  </div>\n  <div class=\"col-md-3 offset-md-9\">\n    <a class=\"link_neuracore_style\" href=\"/#/stories\">STORIES</a>\n  </div>\n\n</div>\n<br><br><br><br>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/services/tech/tech.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TechComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TechComponent = /** @class */ (function () {
    function TechComponent() {
    }
    TechComponent.prototype.ngOnInit = function () {
    };
    TechComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-tech',
            template: __webpack_require__("../../../../../src/app/components/services/tech/tech.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], TechComponent);
    return TechComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/stories/storie/arnic/arnic.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n\n\n<div class=\"  container\">\n\n<div class=\"row portada portada_neura cont_item\">\n  <div class=\"col-md-6 \">\n    <h2 class=\"mth\">ARNIC</h2>\n    <h4><span style=\"font-size: 16px; text-transform: uppercase;\" class=\"core\">Branding</span>  </h4>\n  </div>\n\n  <div class=\"col-md-6 \">\n\n    <p>\n      La presencia es un concepto que es muy iimportante\npara cualquier empresa y tener un sitio dedicado a la\npromoción de los servicios que se ofrecen es crucial\npara impulsar las ventas de cualquier negocio,\nespecialmente en uno tan competido como los\nservicios profesionales.\n    </p>\n  </div>\n\n</div>\n\n<div class=\"row\">\n  <div class=\"col-md-6\">\n    <p>El cliente de este proyecto es una empresa dedicada a\nla prestación de servicios profesionales de distintos\nrubros y especialidades, con un enfoque mayor a\natender a empresas.</p>\n  </div>\n</div>\n\n<div class=\"row justify-content-end\">\n  <div class=\"col-md-6 offset-md-1\">\n  <p>El objetivo de este proyecto fue crear una página web que sirviera\ncomo ventana para dar a conocer los servicios que esta empresa\nofrece de la manera más práctica y llamativa que fuera posible.</p>\n<p>La información debía ser accesible y directa de tal forma que los\nusuarios no tuvieran problemas para encontrar lo que necesitaban\nsaber.</p>\n  </div>\n</div>\n\n\n<div class=\"\">\n  <img src=\"../../../../assets/img/stories/arnic_storie-1.png\" width=\"100%\" alt=\"\">\n</div>\n\n\n<div class=\"row justify-content-end\">\n\n  <div class=\"col-md-6\">\n <br>\n    <p>Para encontrar la mejor forma de presentar la información e\n    impactar a los usuarios de la página, se realizaron diversas\n    propuestas y pruebas.</p>\n    <p>El resultado final fue una página en la que se presenta la\n    información dentro de un solo lienzo, de forma que la información\n    es clara, consisa y resumida. Permite al usuario encontrar todo lo\n    que requiere saber sobre la empresa y ayuda a realizar el contacto\n    con esta de manera simple y rápida.</p>\n  </div>\n</div>\n\n<div class=\"\">\n  <img src=\"../../../../assets/img/stories/arnic_storie_2.gif\" width=\"100%\" alt=\"\">\n</div>\n\n<div class=\"col-md-6\">\n<p>Para completar el proyecto se realizaron distintas aplicaciones de\nmarca, hechas a medida de las necesidades operativas y\ncomerciales de la empresa.</p>\n\n<p>\nPara este proyecto únicamente se realizaron el diseño de logotipo y\nel diseño de algunas aplicaciones impresas</p>\n</div>\n\n\n<div class=\"row justify-content-end\">\n\n  <div class=\"col-md-4\">\n    <div class=\"\">\n      <img src=\"../../../../assets/img/stories/arnic_storie_3.gif\" width=\"100%\" alt=\"\">\n    </div> <br>\n  </div>\n\n</div>\n\n\n <br>\n<div class=\"row\">\n\n  <div class=\"col-md-7\"><br>\n    <p>Para mejorar la experiencia de usuario y ayudar a que la empresa\ntuviera presencia y fuera visible en la mayor cantidad de sitios\nposibles, se realizó un diseño responsivo, optmizando el sitio para\nabrirse en distintos dispositivos.</p>\n  </div>\n</div>\n\n\n\n<div class=\"col-md-8\">\n  <p>Para conocer el sitio completo entra a <a href=\"http://arnic.com.mx/\" target=\"_blank\"> <span class=\"red\">arnic.com.mx</span> </a></p>\n</div>\n\n\n\n</div>\n</div>\n<div class=\"col-md-3 offset-md-9\">\n  <a class=\"link_neuracore_style\" href=\"/#/stories\">STORIES</a>\n</div>\n<br><br><br><br>\n"

/***/ }),

/***/ "../../../../../src/app/components/stories/storie/arnic/arnic.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ArnicComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ArnicComponent = /** @class */ (function () {
    function ArnicComponent() {
    }
    ArnicComponent.prototype.ngOnInit = function () {
    };
    ArnicComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-arnic',
            template: __webpack_require__("../../../../../src/app/components/stories/storie/arnic/arnic.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], ArnicComponent);
    return ArnicComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/stories/storie/bitlegends/bitlegends.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  bitlegends works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/components/stories/storie/bitlegends/bitlegends.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BitlegendsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BitlegendsComponent = /** @class */ (function () {
    function BitlegendsComponent() {
    }
    BitlegendsComponent.prototype.ngOnInit = function () {
    };
    BitlegendsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-bitlegends',
            template: __webpack_require__("../../../../../src/app/components/stories/storie/bitlegends/bitlegends.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], BitlegendsComponent);
    return BitlegendsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/stories/storie/copival/copival.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  copival works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/components/stories/storie/copival/copival.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CopivalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CopivalComponent = /** @class */ (function () {
    function CopivalComponent() {
    }
    CopivalComponent.prototype.ngOnInit = function () {
    };
    CopivalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-copival',
            template: __webpack_require__("../../../../../src/app/components/stories/storie/copival/copival.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], CopivalComponent);
    return CopivalComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/stories/storie/lakshmi/lakshmi.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n\n\n<div class=\"  container\">\n\n<div class=\"row portada portada_neura cont_item\">\n  <div class=\"col-md-6 \">\n    <h2 class=\"mth\">LAKSHMI</h2>\n    <h4><span style=\"font-size: 16px; text-transform: uppercase;\" class=\"core\">BRANDING</span>  </h4>\n  </div>\n\n  <div class=\"col-md-6 \">\n\n    <p>\n      La industria del servicio de manufactura en México es\nuna de las más competidas actualmente, por lo que\ntener un excelente primer contacto es muy importante\npara tener éxito en este negocio.\n\n    </p>\n  </div>\n\n</div>\n\n<div class=\"row\">\n  <div class=\"col-md-6\">\n    <p>El cliente de este proyecto es una startup mexicana\ndedicada al servicio de administración de maquila de\npiezas metálicas en procesos que como corte y doblez\nde lámina metálica, maquinados cnc de alta\ncomplejidad, punzonado, troquelado y acabados\nsuperficiales.</p>\n  </div>\n</div>\n\n<div class=\"row justify-content-end\">\n  <div class=\"col-md-6 offset-md-1\">\n  <p>El objetivo principa fue crear una marca a paritir del nombre\nproporcionado por el cliente, de forma que el resultado final\nproyectara la calidad y seriedad del servicio que presta la empresa.</p>\n<p>\n  De igual forma se tuvieron que tomar en cuenta aspectos como la\npracticidad de implementación de la marca en distintos materiales y\nacabados.</p>\n  </div>\n</div>\n\n\n<div class=\"\">\n  <img src=\"../../../../assets/img/stories/lakshmi_storie-1.png\" width=\"100%\" alt=\"\">\n</div>\n\n<div class=\"col-md-6\">\n<p>Se realizaron distintas propuestas tanto de símbología como de\ncolores y tipografía, tomando en cuenta el mensaje que se quería\nproyectar, así como el significado del nombre de la marca de forma\nque el símbolo fuera lo más acorde al giro del negocio.</p>\n\n\n</div>\n\n<div class=\"row justify-content-end\">\n\n  <div class=\"col-md-7\">\n    <div class=\"\">\n      <img src=\"../../../../assets/img/stories/lakshmi_storie-2.png\" width=\"100%\" alt=\"\">\n    </div> <br>\n  <p>El resultado final fue un logotipo compuesto por un símbolo que\nrepresenta la letra M y P de METALPLEX, integradas en el patrón\nde corte dentro de la abstracción de una lámina cortada en láser.</p>\n  <p>Se eligió una paleta de colores sobria para fortalecer el mensaje de\nseriedad y compromiso, así como para hacer alusión a “Placa de\nMetal” que es el significado del nombre de la marca.</p>\n  </div>\n</div>\n\n<div class=\"col-md-5\">\n  <div class=\"\">\n    <img src=\"../../../../assets/vectors/l_lakshmi_color.svg\" width=\"100%\" alt=\"\">\n  </div> <br>\n</div>\n\n<div class=\"row justify-content-end\">\n\n  <div class=\"col-md-7\">\n\n  <p>Para completar el proyecto se realizaron distintas aplicaciones\nimpresas en distintos materiales. Todas fueron hechas a medida de\nlas necesidades operativas y comerciales de la empresa.</p>\n  </div>\n</div>\n\n<div class=\"row\">\n  <div class=\"col-md-6\">\n    <img src=\"../../../../assets/img/stories/lakshmi_storie-4.png\" width=\"100%\" alt=\"\">\n  </div>\n  <div class=\"col-md-6\">\n    <img src=\"../../../../assets/img/stories/lakshmi_storie_6.png\" width=\"100%\" alt=\"\">\n  </div>\n  </div>\n <br>\n\n<div class=\"row\">\n  <div class=\"col-md-6\">\n    <img src=\"../../../../assets/img/stories/lakshmi_storie_7.png\" width=\"100%\" alt=\"\">\n  </div>\n  <div class=\"col-md-6\">\n    <img src=\"../../../../assets/img/stories/lakshmi_storie_8.png\" width=\"100%\" alt=\"\">\n  </div>\n  </div>\n  <br>\n  <div class=\"\">\n    <img src=\"../../../../assets/img/stories/lakshmi_storie_9.png\" width=\"100%\" alt=\"\">\n  </div>\n   <br>\n\n <br>\n\n\n\n</div>\n</div>\n<div class=\"col-md-3 offset-md-9\">\n  <a class=\"link_neuracore_style\" href=\"/#/stories\">STORIES</a>\n</div>\n<br><br><br><br>\n"

/***/ }),

/***/ "../../../../../src/app/components/stories/storie/lakshmi/lakshmi.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LakshmiComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LakshmiComponent = /** @class */ (function () {
    function LakshmiComponent() {
    }
    LakshmiComponent.prototype.ngOnInit = function () {
    };
    LakshmiComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-lakshmi',
            template: __webpack_require__("../../../../../src/app/components/stories/storie/lakshmi/lakshmi.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], LakshmiComponent);
    return LakshmiComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/stories/storie/metalplex/metalplex.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n\n\n<div class=\"  container\">\n\n<div class=\"row portada portada_neura cont_item\">\n  <div class=\"col-md-6 \">\n    <h2 class=\"mth\">METALPLEX</h2>\n    <h4><span style=\"font-size: 16px; text-transform: uppercase;\" class=\"core\">BRANDING</span>  </h4>\n  </div>\n\n  <div class=\"col-md-6 \">\n\n    <p>\n      La industria del servicio de manufactura en México es\nuna de las más competidas actualmente, por lo que\ntener un excelente primer contacto es muy importante\npara tener éxito en este negocio.\n\n    </p>\n  </div>\n\n</div>\n\n<div class=\"row\">\n  <div class=\"col-md-6\">\n    <p>El cliente de este proyecto es una startup mexicana\ndedicada al servicio de administración de maquila de\npiezas metálicas en procesos que como corte y doblez\nde lámina metálica, maquinados cnc de alta\ncomplejidad, punzonado, troquelado y acabados\nsuperficiales.</p>\n  </div>\n</div>\n\n<div class=\"row justify-content-end\">\n  <div class=\"col-md-6 offset-md-1\">\n  <p>El objetivo principa fue crear una marca a paritir del nombre\nproporcionado por el cliente, de forma que el resultado final\nproyectara la calidad y seriedad del servicio que presta la empresa.</p>\n<p>\n  De igual forma se tuvieron que tomar en cuenta aspectos como la\npracticidad de implementación de la marca en distintos materiales y\nacabados.</p>\n  </div>\n</div>\n\n\n<div class=\"\">\n  <img src=\"../../../../assets/img/stories/metalplex_storie-1.png\" width=\"100%\" alt=\"\">\n</div>\n\n<div class=\"col-md-6\">\n<p>Se realizaron distintas propuestas tanto de símbología como de\ncolores y tipografía, tomando en cuenta el mensaje que se quería\nproyectar, así como el significado del nombre de la marca de forma\nque el símbolo fuera lo más acorde al giro del negocio.</p>\n\n\n</div>\n\n<div class=\"row justify-content-end\">\n\n  <div class=\"col-md-6\">\n    <div class=\"\">\n      <img src=\"../../../../assets/img/stories/metalplex_storie-2.png\" width=\"100%\" alt=\"\">\n    </div> <br>\n  <p>El resultado final fue un logotipo compuesto por un símbolo que\nrepresenta la letra M y P de METALPLEX, integradas en el patrón\nde corte dentro de la abstracción de una lámina cortada en láser.</p>\n  <p>Se eligió una paleta de colores sobria para fortalecer el mensaje de\nseriedad y compromiso, así como para hacer alusión a “Placa de\nMetal” que es el significado del nombre de la marca.</p>\n  </div>\n</div>\n\n<div class=\"col-md-6\">\n  <div class=\"\">\n    <img src=\"../../../../assets/vectors/l_metalplex_color.svg\" width=\"100%\" alt=\"\">\n  </div> <br>\n</div>\n\n<div class=\"row justify-content-end\">\n\n  <div class=\"col-md-6\">\n\n  <p>Para completar el proyecto se realizaron distintas aplicaciones\nimpresas en distintos materiales. Todas fueron hechas a medida de\nlas necesidades operativas y comerciales de la empresa.</p>\n  </div>\n</div>\n\n<div class=\"\">\n  <img src=\"../../../../assets/img/stories/metalplex_storie-3.png\" width=\"100%\" alt=\"\">\n</div>\n <br>\n\n<div class=\"row\">\n  <div class=\"col-md-6\">\n    <img src=\"../../../../assets/img/stories/metalplex_storie-4.png\" width=\"100%\" alt=\"\">\n  </div>\n  <div class=\"col-md-6\">\n    <img src=\"../../../../assets/img/stories/metalplex_storie-5.png\" width=\"100%\" alt=\"\">\n  </div>\n  </div>\n  <br>\n  <div class=\"\">\n    <img src=\"../../../../assets/img/stories/metalplex_storie-6.png\" width=\"100%\" alt=\"\">\n  </div>\n   <br>\n\n\n\n <br>\n\n\n\n\n\n</div>\n</div>\n<div class=\"col-md-3 offset-md-9\">\n  <a class=\"link_neuracore_style\" href=\"/#/stories\">STORIES</a>\n</div>\n<br><br><br><br>\n"

/***/ }),

/***/ "../../../../../src/app/components/stories/storie/metalplex/metalplex.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MetalplexComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MetalplexComponent = /** @class */ (function () {
    function MetalplexComponent() {
    }
    MetalplexComponent.prototype.ngOnInit = function () {
    };
    MetalplexComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-metalplex',
            template: __webpack_require__("../../../../../src/app/components/stories/storie/metalplex/metalplex.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], MetalplexComponent);
    return MetalplexComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/stories/storie/mm/mm.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  mm works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/components/stories/storie/mm/mm.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MmComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MmComponent = /** @class */ (function () {
    function MmComponent() {
    }
    MmComponent.prototype.ngOnInit = function () {
    };
    MmComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-mm',
            template: __webpack_require__("../../../../../src/app/components/stories/storie/mm/mm.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], MmComponent);
    return MmComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/stories/storie/sahlo/sahlo.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n\n\n<div class=\"  container\">\n\n<div class=\"row portada portada_neura cont_item\">\n  <div class=\"col-md-6 \">\n    <h2 class=\"mth\">SAHLO</h2>\n    <h4><span style=\"font-size: 16px; text-transform: uppercase;\" class=\"core\">Branding</span>  </h4>\n  </div>\n\n  <div class=\"col-md-6 \">\n\n    <p>\n      La capacitación e implementación de sistemas de\ncontrol de calidad y normatividad en la industria es una\nnecesidad latente en las empresas de manufactura, así\ncomo el cuidado del ambiente.\nSin embargo los clientes para este rubro requieren de\nempresas que inspiren confianza por lo delicado de las\nimplicaciones de estos servicios.\n    </p>\n  </div>\n\n</div>\n\n<div class=\"row\">\n  <div class=\"col-md-6\">\n    <p>El cliente de este proyecto es una startup mexicana\ndedicada al servicio de capacitación e implementación\nde herramientas y normas para optimizar los procesos y\nmejorar la calidad de los servicios de otras empresas.</p>\n  </div>\n</div>\n\n<div class=\"row justify-content-end\">\n  <div class=\"col-md-6 offset-md-1\">\n  <p>El objetivo principa fue crear una marca que proyectara confianza y\nseriedad, así como un aire corporativo, de forma que los clientes de\nesta startup tuvieran un primer contacto ótpimo que atraiga su\natención y sirva como herramienta para impulsar el\nposicionamiento de la marca.</p>\n  </div>\n</div>\n\n\n<div class=\"\">\n  <img src=\"../../../../assets/img/stories/sahlo_storie_1.png\" width=\"100%\" alt=\"\">\n</div>\n\n<div class=\"col-md-6\">\n<p>Se realizaron distintas propuestas tanto de símbología como de\ncolores y tipografía, tomando en cuenta el mensaje que se quería\nproyectar de forma que fuera lo más adecuado a los\nrequerimientos iniciales.</p>\n\n\n</div>\n\n<div class=\"row justify-content-end\">\n\n  <div class=\"col-md-6\">\n\n  <p>El resultado final fue un isologo, lo que significa que el símbolo está\nincluido y forma parte del texto del logotipo.</p>\n<p>Este esta compuesto por los elementos tipográficos y el símbolo\nque funge como sustitución de la letra “O”, el símbolo representa los\nvalores de la empresa al abstrær la manera en la que acoge o\nabraza a sus clientes y los ayuda a alcanzar su mayor potencial.</p>\n<p>Los colores fueron seleccionados para proyectar formalidad y\nconfianza de manera que todo el mensaje se complementa.</p>\n  </div>\n</div>\n\n<div class=\"col-md-6\">\n  <div class=\"\">\n    <img src=\"../../../../assets/vectors/l_sahlo_color.svg\" width=\"100%\" alt=\"\">\n  </div> <br>\n</div>\n\n<div class=\"row justify-content-end\">\n\n  <div class=\"col-md-6\">\n\n  <p>Para completar el proyecto se realizaron distintas aplicaciones tanto\nimpresas como digitales. Todas medida de las necesidades\noperativas y comerciales de la empresa.</p>\n  </div>\n</div>\n\n\n <br>\n<div class=\"row\">\n  <div class=\"col-md-6\">\n    <img src=\"../../../../assets/img/stories/sahlo_storie_2.png\" width=\"100%\" alt=\"\">\n  </div>\n  <div class=\"col-md-6\">\n    <img src=\"../../../../assets/img/stories/sahlo_storie_3.png\" width=\"100%\" alt=\"\">\n  </div>\n</div>\n<br>\n<div class=\"row\">\n  <div class=\"col-md-6\">\n    <img src=\"../../../../assets/img/stories/sahlo_storie_4.png\" width=\"100%\" alt=\"\">\n  </div>\n  <div class=\"col-md-6\">\n    <img src=\"../../../../assets/img/stories/sahlo_storie_5.png\" width=\"100%\" alt=\"\">\n  </div>\n</div>\n\n\n <br>\n\n<div class=\"col-md-8\">\n  <p>Para conocer más acerca de esta empresa puedes visitarlos en su\nweb <a href=\"http://sahlo.mx/\" target=\"_blank\"> <span class=\"red\">sahlo.mx</span> </a> y en sus redes sociales.</p>\n</div>\n\n\n\n</div>\n</div>\n\n<div class=\"col-md-3 offset-md-9\">\n  <a class=\"link_neuracore_style\" href=\"/#/stories\">STORIES</a>\n</div>\n<br><br><br><br>\n"

/***/ }),

/***/ "../../../../../src/app/components/stories/storie/sahlo/sahlo.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SahloComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SahloComponent = /** @class */ (function () {
    function SahloComponent() {
    }
    SahloComponent.prototype.ngOnInit = function () {
    };
    SahloComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-sahlo',
            template: __webpack_require__("../../../../../src/app/components/stories/storie/sahlo/sahlo.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], SahloComponent);
    return SahloComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/stories/storie/spiglocase/spiglocase.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n\n\n<div class=\"  container\">\n\n<div class=\"row portada portada_neura cont_item\">\n  <div class=\"col-md-6 \">\n    <h2 class=\"mth\">SPIGLOCASE</h2>\n    <h4><span style=\"font-size: 16px; text-transform: uppercase;\" class=\"core\">DESARROLLO DE PRODUCTO</span>  </h4>\n  </div>\n\n  <div class=\"col-md-6 \">\n\n    <p>\n      Surge de la necesidad de <span class=\"red\">crea</span> un accesorio práctico e\ninnovador que permita al usuario realizar tareas con su\nteléfono celular permitiendo que las dos manos queden\nlibres para realizar otras actividades.\n\n    </p>\n  </div>\n\n</div>\n\n<div class=\"row\">\n  <div class=\"col-md-6\">\n    <p>El cliente de este proyecto es una startup mexicana\ndedicada a la fabricación y comercialización de\naccesorios para celulares.</p>\n  </div>\n</div>\n\n<div class=\"row justify-content-end\">\n  <div class=\"col-md-6 offset-md-1\">\n  <p>El objetivo de proyecto fue crear un accesorio que permitiera al\nusuario colocar su celular en el dorso de la mano, con la finalidad\nde permitirle realizar otras actividades mientras lo sostiene.</p>\n<p>\n  Algunos de los requerimientos que se solicitaron fueron que el\n  producto permitiera al usuario girar el teléfono en dos ejes para\n  posicionarlo y permitirle además de ver su pantalla, tomar fotogafías\n  con un control bluetooth de “selfiestick” comercial, que hiciera sentir\n  al usuario seguro y confiado de colocar su teléfono y que pudiera\n  ser fabricado a escala industrial para alcanzar un mercado más\n  amplio.</p>\n  </div>\n</div>\n\n\n<div class=\"\">\n  <img src=\"../../../../assets/img/stories/spiglocase_storie_sketch.png\" width=\"100%\" alt=\"\">\n</div>\n\n<div class=\"col-md-6\">\n<p>Durante el proyecto se realizaron algunas propuestas de solución\npara la problemática presentada, se exploraron distintas formas de\nsostener el celular y soluciones para la articulación que permitiría\nrealizar las tareas que se solicitaron en los requerimientos iniciales\ndel proyecto.</p>\n\n\n</div>\n\n<div class=\"row justify-content-end\">\n\n  <div class=\"col-md-6\">\n    <div class=\"\">\n      <img src=\"../../../../assets/img/stories/spiglocase_storie_process.png\" width=\"100%\" alt=\"\">\n    </div> <br>\n  <p>Durante el proceso se realizaron distintas pruebas, modelos y\nprototipos que sirvieron para probar la funcionalidad, ergonomía,\npracticidad y estética del producto, así como la facilidad y viabilidad\nde producción.</p>\n  </div>\n</div>\n\n<div class=\"col-md-12\">\n  <div class=\"\">\n    <img src=\"../../../../assets/img/stories/spiglocase_storie_develop.png\" width=\"100%\" alt=\"\">\n  </div> <br>\n</div>\n\n<div class=\"row justify-content-end\">\n\n  <div class=\"col-md-6\">\n\n  <p>De esta forma el resultado final fue un guante con un estilo\nminimalista adecuado para todo público, pensado para no interferir\ncon las actividades del usuario y realizado con los criterios de\nergonomía que permiten que el usuario se sienta cómodo y seguro\nal usar el producto.</p>\n  </div>\n</div>\n\n<div class=\"\">\n  <img src=\"../../../../assets/img/banner_home.png\" width=\"100%\" alt=\"\">\n</div>\n <br>\n\n<div class=\"row\">\n  <div class=\"col-md-6\">\n    <img src=\"../../../../assets/img/stories/spiglocase_storie_1.png\" width=\"100%\" alt=\"\">\n  </div>\n  <div class=\"col-md-6\">\n    <img src=\"../../../../assets/img/stories/spiglocase_storie_2.png\" width=\"100%\" alt=\"\">\n  </div>\n  </div>\n  <br>\n  <div class=\"\">\n    <img src=\"../../../../assets/img/stories/spiglocase_storie_3.png\" width=\"100%\" alt=\"\">\n  </div>\n   <br>\n  <div class=\"row\">\n    <div class=\"col-md-6\">\n      <img src=\"../../../../assets/img/stories/spiglocase_storie_4.png\" width=\"100%\" alt=\"\">\n    </div>\n    <div class=\"col-md-6\">\n      <img src=\"../../../../assets/img/stories/spiglocase_storie_5.png\" width=\"100%\" alt=\"\">\n    </div>\n</div>\n\n\n <br>\n\n<div class=\"col-md-8\">\n  <p>Al termino del proyecto se entregaron toda documentación técnica\ny archivos necesarios para la fabricación del producto, así como un\nprototipo final.</p>\n</div>\n\n\n\n</div>\n</div>\n<div class=\"col-md-3 offset-md-9\">\n  <a class=\"link_neuracore_style\" href=\"/#/stories\">STORIES</a>\n</div>\n<br><br><br><br>\n"

/***/ }),

/***/ "../../../../../src/app/components/stories/storie/spiglocase/spiglocase.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpiglocaseComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SpiglocaseComponent = /** @class */ (function () {
    function SpiglocaseComponent() {
    }
    SpiglocaseComponent.prototype.ngOnInit = function () {
    };
    SpiglocaseComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-spiglocase',
            template: __webpack_require__("../../../../../src/app/components/stories/storie/spiglocase/spiglocase.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], SpiglocaseComponent);
    return SpiglocaseComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/stories/storie/storie.component.html":
/***/ (function(module, exports) {

module.exports = "\n<div class=\"container\">\n\n<div class=\"portada row \">\n  <div class=\"col-md-5 offset-md-1 \">\n    <h2 class=\"mth\">MAKING<br>THINGS<br>HAPPEN</h2>\n  </div>\n  <div class=\"col-md-6\">\n    <p> <br>\n      El mundo está repleto de proyectos e ideas extraordinarias. Sin embargo por distintas razones no\n      son desarrolladas, perdiendo su potencial.\n    </p>\n    <p>\n      Bienvenido a <span class=\"neura\">NEURA</span><span class=\"core\">CORE</span> <span class=\"studio\">STUDIO</span> el lugar en donde tus ideas alcanzan todo su potencial.\n    </p>\n  </div>\n  <div class=\"col-md-6 offset-md-6\">\n    <button type=\"button\" class=\"button_neuracore_style\" name=\"button\">TENGO UNA IDEA</button>\n  </div>\n</div>\n\n<br><br><br><br>\n\n<div class=\"row\">\n  <div class=\"col-md-5\">\n    <p>Somos una agencia de <span class=\"red\">INNOVACIÓN</span> ubicada en la\n    CIUDAD DE MÉXICO.\n\n    A través del trabajo multidisciplinario ayudamos a\n    empresas, empresarios y emprendedores a consguir sus\n  metas y hacer realidad sus proyectos.</p>\n  </div>\n  <div class=\"col-md-6\">\n\n  </div>\n</div>\n\n</div>\n<!-- End Portada -->\n\n<!-- Banner sin container -->\n\n<img src=\"../../../../assets/img/banner_home.png\" width=\"100%\" alt=\"...\">\n<br><br><br><br><br><br>\n\n<div class=\"container\">\n<!-- METODOLOGÍA -->\n  <div class=\"row justify-content-end\">\n\n    <div class=\"col-md-6\">\n      <p>Para garantizar la calidad del serivicio, conseguir\n        cumplir con los tiempos de entrega y garantizar los\n        requerimientos de los proyectos hemos desarrollado\n        nuestra <span class=\"red\">METODOLOGÍA</span> que nos permite llegar\n        de una idea a un proyecto final viable.</p>\n    </div>\n  </div>\n\n  <div class=\"row\">\n    <div class=\"col-md-6\">\n      <!-- la clase pull-xs-left no se que sea la neta, se puso sola y la deje :) -->\n      <img src=\"../../../../assets/vectors/metodo_neuracorestudio.svg\" class=\"img-fluid pull-xs-left\" alt=\"...\">\n    </div>\n    <div class=\"col-md-6\">\n\n    </div>\n    </div>\n\n    <div class=\"row justify-content-end\">\n      <div class=\"col-md-6\">\n        <p>Todo nuestro proceso es cíclico y está estructurado en etapas que pueden realizarse de manera individual, lo que significa que podemos iniciar en cualquiera de ellas y regresarnos en caso de necesidades particulæs de tu proyecto.</p>\n      </div>\n\n  </div>\n\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/stories/storie/storie.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StorieComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__servicios_stories_service__ = __webpack_require__("../../../../../src/app/servicios/stories.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var StorieComponent = /** @class */ (function () {
    function StorieComponent(activatedRoute, _sotriesService) {
        var _this = this;
        this.activatedRoute = activatedRoute;
        this._sotriesService = _sotriesService;
        this.storie = {};
        this.activatedRoute.params.subscribe(function (params) {
            // console.log(params);
            _this.storie = _this._sotriesService.getStorie(params['id']);
        });
    }
    StorieComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-storie',
            template: __webpack_require__("../../../../../src/app/components/stories/storie/storie.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_2__servicios_stories_service__["a" /* StoriesService */]])
    ], StorieComponent);
    return StorieComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/stories/storie/temika/temika.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn\">\n\n\n<div class=\"  container\">\n\n<div class=\"row portada portada_neura cont_item\">\n  <div class=\"col-md-6 \">\n    <h2 class=\"mth\">TEMIKA CONSULT</h2>\n    <h4><span style=\"font-size: 16px; text-transform: uppercase;\" class=\"core\">Branding</span>  </h4>\n  </div>\n\n  <div class=\"col-md-6 \">\n\n    <p>\n      Los “emprendedores” son un mercado creciente ya que\ntodos los días se crean nuevas empresas, a raíz de esta\nde necesidad se <span class=\"red\">crea</span> esta marca, dedicada a ayudar\nemprendedores y pequeñas empresas con sus\naspectos legales.\n    </p>\n  </div>\n\n</div>\n\n<div class=\"row\">\n  <div class=\"col-md-6\">\n    <p>El cliente de este proyecto es un despacho de\nlicenciados dedicados a los temas administrativos,\nAnteriormente esta empresa tenía el nombre de G y G\nconsultores.</p>\n  </div>\n</div>\n\n<div class=\"row justify-content-end\">\n  <div class=\"col-md-6 offset-md-1\">\n  <p>El objetivo de este proyecto fue crear una marca que ayudara a\ngenerar confianza en sus consumidores, considerando la mala\npercepción que los profesionales del derecho tienen en nuestro\npaís.\nOtro de los requerimientos fue generar una personalidad de marca\nque tuviera un equilibrio entre la formalidad y la calidez de una\nempresa que está dispuesta a ayudar y cuidar a sus clientes, de\nforma que todo el proyecto se enfocó en satisfacer estos\nrequerimientos.</p>\n  </div>\n</div>\n\n\n<div class=\"\">\n  <img src=\"../../../../assets/img/stories/temika_c_storie_sketch.png\" width=\"100%\" alt=\"\">\n</div>\n\n<div class=\"col-md-6\">\n<p>Durante el proyecto se detectó la necesidad de realizar un cambio\nde nombre, con la finalidad de generar una mejor atracción y ayudar\na que fuera sencillo de recordar por los consumidores.</p>\n\n<p>Después de esto se realizaron diversas propuestas tanto de\nsímbología como de colores, tomando en cuenta el mensaje que\nse quería mandar y algunas de las creencias de nuestros clientes.</p>\n</div>\n\n<div class=\"row justify-content-end\">\n\n  <div class=\"col-md-6\">\n    <div class=\"\">\n      <img src=\"../../../../assets/img/stories/temika_c_storie_logo.png\" width=\"100%\" alt=\"\">\n    </div> <br>\n  <p>El objetivo de este proyecto fue crear una marca que ayudara a\ngenerar confianza en sus consumidores, considerando la mala\npercepción que los profesionales del derecho tienen en nuestro\npaís.\nOtro de los requerimientos fue generar una personalidad de marca\nque tuviera un equilibrio entre la formalidad y la calidez de una\nempresa que está dispuesta a ayudar y cuidar a sus clientes, de\nforma que todo el proyecto se enfocó en satisfacer estos\nrequerimientos.</p>\n  </div>\n</div>\n\n<div class=\"col-md-6\">\n  <div class=\"\">\n    <img src=\"../../../../assets/vectors/l_temikaconsult_color.svg\" width=\"100%\" alt=\"\">\n  </div> <br>\n</div>\n\n<div class=\"row justify-content-end\">\n\n  <div class=\"col-md-6\">\n\n  <p>Para completar el proyecto se realizaron distintas aplicaciones de\nmarca tanto impresas como digitales, hechas a medida de las\nnecesidades operativas y comerciales de la empresa.</p>\n  </div>\n</div>\n\n<div class=\"\">\n  <img src=\"../../../../assets/img/stories/temika_c_storie_1.png\" width=\"100%\" alt=\"\">\n</div>\n <br>\n<div class=\"row\">\n  <div class=\"col-md-6\">\n    <img src=\"../../../../assets/img/stories/temika_c_storie_2.png\" width=\"100%\" alt=\"\">\n  </div>\n  <div class=\"col-md-6\">\n    <img src=\"../../../../assets/img/stories/temika_c_storie_3.png\" width=\"100%\" alt=\"\">\n  </div>\n  <div class=\"col-md-7\"><br>\n    <p>Además de las aplicaciones de marca se realizó el diseño de\npágina web y la creación de perfiles de redes sociales, de esta\nforma el impacto de la marca se hace en todos los puntos de\ncontacto de la empresa.</p>\n<p>Así mismo se generó un manual de identidad de marca que sirve\ncomo guía para posteriores aplicaciones de la misma.</p>\n  </div>\n</div>\n\n<div class=\"\">\n  <img src=\"../../../../assets/img/stories/temika_c_storie_4.png\" width=\"100%\" alt=\"\">\n</div>\n <br>\n\n<div class=\"col-md-8\">\n  <p>Para conocer más acerca de esta empresa puedes visitarlos en su\nweb <a href=\"http://temikaconsult.com/\" target=\"_blank\"> <span class=\"red\">temikaconsult.com</span> </a> y en sus redes sociales.</p>\n</div>\n\n\n\n</div>\n</div>\n\n<div class=\"col-md-3 offset-md-9\">\n  <a class=\"link_neuracore_style\" href=\"/#/stories\">STORIES</a>\n</div>\n<br><br><br><br>\n"

/***/ }),

/***/ "../../../../../src/app/components/stories/storie/temika/temika.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TemikaComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TemikaComponent = /** @class */ (function () {
    function TemikaComponent() {
    }
    TemikaComponent.prototype.ngOnInit = function () {
    };
    TemikaComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-temika',
            template: __webpack_require__("../../../../../src/app/components/stories/storie/temika/temika.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], TemikaComponent);
    return TemikaComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/stories/storie/tenorio/tenorio.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  tenorio works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/components/stories/storie/tenorio/tenorio.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TenorioComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TenorioComponent = /** @class */ (function () {
    function TenorioComponent() {
    }
    TenorioComponent.prototype.ngOnInit = function () {
    };
    TenorioComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-tenorio',
            template: __webpack_require__("../../../../../src/app/components/stories/storie/tenorio/tenorio.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], TenorioComponent);
    return TenorioComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/stories/storie/virtef/virtef.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  virtef works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/components/stories/storie/virtef/virtef.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VirtefComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var VirtefComponent = /** @class */ (function () {
    function VirtefComponent() {
    }
    VirtefComponent.prototype.ngOnInit = function () {
    };
    VirtefComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-virtef',
            template: __webpack_require__("../../../../../src/app/components/stories/storie/virtef/virtef.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], VirtefComponent);
    return VirtefComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/stories/stories.component.html":
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n\n<div class=\"animated fadeIn container\">\n\n\n  <div class=\"portada portada_neura cont_item row \">\n    <div class=\"col-md-5 offset-md-1 \">\n      <h2 class=\"mth\">STORIES</h2>\n      <h4><span style=\"font-size: 16px; text-transform: uppercase;\" class=\"core\">by:</span>  <span class=\"neura\">NEURA</span><span class=\"core\">CORE</span> <span class=\"studio\">STUDIO</span></h4>\n    </div>\n    <div class=\"col-md-6\">\n      <p>\n        Lo que hace grande a las empresas es  su determinación para alcanzar sus objetivos. Nuestros clientes tienen esa determinaciòn; ellos lo sueñan y lo imaginan, nosotros lo hacemos realidad.\n      </p>\n    </div>\n  </div>\n\n  <br/><br/><br/><br/>\n<div class=\"card-columns\">\n<!-- Cards de boostra insertadas mediante un servicio de angular, con un tipo json en stories.service-->\n  <div class=\"card img_card\" *ngFor=\"let storie of stories; let i = index\" >\n    <a class=\"img_card\" href=\"/#/{{storie.link}}\">\n    <img class=\"card-img-top\" [src]=\"storie.image\" [alt]=\"storie.title\"></a>\n    <div class=\"card-body\">\n      <a class=\"title_card\" href=\"/#/{{storie.link}}\">\n      <h5>{{storie.title}}</h5></a>\n      <a class=\"text_card\" href=\"/#/{{storie.link}}\">\n      <p>{{storie.abstract}}</p></a>\n      <br />\n    </div>\n  </div>\n</div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/stories/stories.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StoriesComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__servicios_stories_service__ = __webpack_require__("../../../../../src/app/servicios/stories.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var StoriesComponent = /** @class */ (function () {
    function StoriesComponent(_storiesService) {
        this._storiesService = _storiesService;
        this.stories = [];
    }
    StoriesComponent.prototype.ngOnInit = function () {
        this.stories = this._storiesService.getStories();
    };
    StoriesComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-stories',
            template: __webpack_require__("../../../../../src/app/components/stories/stories.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__servicios_stories_service__["a" /* StoriesService */]])
    ], StoriesComponent);
    return StoriesComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/talks/talk/talk.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  talk works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/components/talks/talk/talk.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TalkComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__servicios_talks_service__ = __webpack_require__("../../../../../src/app/servicios/talks.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TalkComponent = /** @class */ (function () {
    function TalkComponent(activatedRoute, _talksService) {
        var _this = this;
        this.activatedRoute = activatedRoute;
        this._talksService = _talksService;
        this.talk = {};
        this.activatedRoute.params.subscribe(function (params) {
            // console.log(params);
            _this.talk = _this._talksService.getTalk(params['id']);
        });
    }
    TalkComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-talk',
            template: __webpack_require__("../../../../../src/app/components/talks/talk/talk.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */],
            __WEBPACK_IMPORTED_MODULE_2__servicios_talks_service__["a" /* TalksService */]])
    ], TalkComponent);
    return TalkComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/talks/talks.component.html":
/***/ (function(module, exports) {

module.exports = "<div  class=\"animated fadeIn container\">\n\n  <div class=\" fadeIn portada_neura cont_item row \">\n    <div class=\"col-md-5 offset-md-1 \">\n      <h2 class=\"mth\">TALKS</h2>\n      <h4> <span style=\"font-size: 16px; text-transform: uppercase;\" class=\"core\">by:</span> <span class=\"neura\">NEURA</span><span class=\"core\">CORE</span> <span class=\"studio\">STUDIO</span></h4>\n    </div>\n    <div class=\"col-md-6\">\n      <p>\n        Aquí podrás encontrar los mejores artículos e información sobre <span class=\"red\">innovación</span>, <span class=\"red\">diseño</span>, <span class=\"red\">tecnología</span> y <span class=\"red\">marketing</span>.\n      </p>\n    </div>\n\n  </div>\n\n  <!-- <br/><br/><br/><br/> -->\n\n  <div class=\" fadeIn portada_neura cont_item \">\n    <div class=\"col-md-5 offset-md-5 \">\n      <h2 class=\"mth\">PRÓXIMAMENTE</h2>\n      <h4> <span style=\"font-size: 16px; text-transform: uppercase;\" class=\"core\">esperalo...</span> </h4>\n    </div>\n\n\n  </div>\n\n  <!-- Cards de boostra insertadas mediante un servicio de angular, con un tipo json en talks.service-->\n\n<!--<div class=\"card-columns\">\n  <div class=\"card img_card \" *ngFor=\"let talk of talks; let i = index\" >\n  <a class=\"\" [routerLink]=\"['/talk',i]\">\n    <img class=\"card-img-top\" [src]=\"talk.image\" [alt]=\"talk.title\"> </a>\n    <div class=\"card-body\">\n      <a class=\"title_card\" [routerLink]=\"['/talk',i]\">\n      <h5>{{talk.title}}</h5></a>\n      <a class=\"text_card\" [routerLink]=\"['/talk',i]\">\n      <p>{{talk.abstract}}</p></a>\n    </div>\n  </div>\n</div>-->\n\n\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/components/talks/talks.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TalksComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__servicios_talks_service__ = __webpack_require__("../../../../../src/app/servicios/talks.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TalksComponent = /** @class */ (function () {
    function TalksComponent(_talksService) {
        this._talksService = _talksService;
        this.talks = [];
    }
    TalksComponent.prototype.ngOnInit = function () {
        this.talks = this._talksService.getTalks();
    };
    TalksComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-talks',
            template: __webpack_require__("../../../../../src/app/components/talks/talks.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__servicios_talks_service__["a" /* TalksService */]])
    ], TalksComponent);
    return TalksComponent;
}());



/***/ }),

/***/ "../../../../../src/app/servicios/stories.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StoriesService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var StoriesService = /** @class */ (function () {
    function StoriesService() {
        this.stories = [
            {
                title: 'SPIGLOCASE',
                abstract: 'Diseño y desarrollo de accesorio para celulares, próximamente a la venta.',
                image: './../assets/img/stories/spiglocase.png',
                link: 'spiglocase'
            },
            {
                title: 'LAKSHMI',
                abstract: 'Branding para marca de bolsas de piel hechas en México.',
                image: './../assets/img/stories/lakshmi.png',
                link: 'lakshmi'
            },
            // {
            //   title: 'VIRTEF',
            //   abstract: 'Diseño de plataforma de finanzas personales y manejo de tarjetas de crédito.',
            //   image: './../assets/img/stories/virtef.png',
            //   link: 'virtef'
            // },
            {
                title: 'TEMIKA CONSULT',
                abstract: 'Branding de firma de licenciados para emprendedores.',
                image: './../assets/img/stories/temika.png',
                link: 'temika'
            },
            // {
            //   title: 'VELVET',
            //   abstract: 'Desarrollo de la primera plataforma de interacción comercial en México.',
            //   image: './../assets/img/stories/velvet.png',
            //   link: 'velvet'
            // },
            // {
            //   title: 'SILLA SONORA',
            //   abstract: 'Diseño, desarrollo y prototipado de Silla para concurso de diseño industrial.',
            //   image: './../assets/img/stories/sillasonora.png',
            //   link: 'sillasonora'
            // },
            {
                title: 'ARNIC',
                abstract: 'Desarrollo de landing page responsiva, para empresa de servicios profesionales.',
                image: './../assets/img/stories/arnic.png',
                link: 'arnic'
            },
            // {
            //   title: 'ELEMENT SHOTS',
            //   abstract: 'Diseño y desarrollo hasta producción piloto de colección de tequileros de cerámica para la marca Bit Legends.',
            //   image: './../assets/img/stories/elementshots.png',
            //   link: 'elementshots'
            // },
            // {
            //   title: 'M+M',
            //   abstract: 'Branding para el despacho de arquitectos M+M, ubicado en Orizaba, Veracruz.',
            //   image: './../assets/img/stories/mm.png',
            //   link: 'mm'
            // },
            // {
            //   title: 'MUSHROOM BOWL',
            //   abstract: 'Diseño y desarrollo hasta producción piloto de utencilios de cocina para Bit Legends.',
            //   image: './../assets/img/stories/mushroombowl.png',
            //   link: 'mushroombowl'
            // },
            {
                title: 'METALPLEX',
                abstract: 'Branding para empresa de servicios de fabricación metalmecánica automatizada.',
                image: './../assets/img/stories/metalplex.png',
                link: 'metalplex'
            },
            {
                title: 'SAHLO',
                abstract: 'Branding para empresa de servicios de consultoría de procesos y mejora continua.',
                image: './../assets/img/stories/sahlo.png',
                link: 'sahlo'
            },
        ];
        console.log('service listo :)');
    }
    StoriesService.prototype.getStories = function () {
        return this.stories;
    };
    StoriesService.prototype.getStorie = function (idx) {
        return this.stories[idx];
    };
    StoriesService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], StoriesService);
    return StoriesService;
}());



/***/ }),

/***/ "../../../../../src/app/servicios/talks.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TalksService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TalksService = /** @class */ (function () {
    function TalksService() {
        this.talks = [
            {
                title: 'Conoce la importancia, función y características de una agencia de innovación',
                abstract: 'La innovación .',
                image: './../assets/img/talks/neura_loading.gif'
            },
            {
                title: 'Branding, más que un logotipo',
                abstract: 'Un desacierto común es creer que el branding es solo la imagen visual de la empresa, se trata de construcción de marca',
                image: './../assets/img/talks/neura_loading.gif'
            },
            {
                title: 'Marketing inbound. Conócelo paso a paso.',
                abstract: 'El Inbound se ha vuelto por excelencia la técnica más novedosa para fidelizar y atraer clientes a tu empresa.',
                image: './../assets/img/talks/neura_loading.gif'
            },
            {
                title: 'Elementos a considerar para crear una página web',
                abstract: 'Existen una serie de elementos importantes que son de gran ayuda para la creación de una página web.',
                image: './../assets/img/talks/neura_loading.gif'
            },
            {
                title: '¿Quieres emprender? Aquí te decimos cómo',
                abstract: 'No es imposible crear tu propia empresa , pero debes organizarte y prepararte para lograr el gran reto',
                image: './../assets/img/talks/neura_loading.gif'
            },
            {
                title: '5 diseñadores mexicanos que no podrás olvidar.',
                abstract: 'Hay mucho talento mexicano. Conoce a estos 5 diseñadores que te lo demostraran.',
                image: './../assets/img/talks/neura_loading.gif'
            }
        ];
        console.log('service listo :)');
    }
    TalksService.prototype.getTalks = function () {
        return this.talks;
    };
    TalksService.prototype.getTalk = function (idx) {
        return this.talks[idx];
    };
    TalksService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], TalksService);
    return TalksService;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: true
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map