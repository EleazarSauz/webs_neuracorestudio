<?php /*%%SmartyHeaderCode:439698647599bb5da2b87b4-80220778%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fa6cc378d2942c8857b89d6bca728048c0caeedd' => 
    array (
      0 => 'module:ps_featuredproducts/views/templates/hook/ps_featuredproducts.tpl',
      1 => 1503374249,
      2 => 'module',
    ),
    '9de44ca81d85727d6c1f718a1b86a6af687791cb' => 
    array (
      0 => '/home/neuracor/public_html/bit-legends/themes/classic/templates/catalog/_partials/miniatures/product.tpl',
      1 => 1503374249,
      2 => 'file',
    ),
    'f6ed3460816bb16d0c257c5e7079f1bb3be14fc5' => 
    array (
      0 => '/home/neuracor/public_html/bit-legends/themes/classic/templates/catalog/_partials/variant-links.tpl',
      1 => 1503374249,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '439698647599bb5da2b87b4-80220778',
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5baaa89e915710_44561438',
  'has_nocache_code' => false,
  'cache_lifetime' => 31536000,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5baaa89e915710_44561438')) {function content_5baaa89e915710_44561438($_smarty_tpl) {?><section class="featured-products clearfix">
  <h1 class="h1 products-section-title text-uppercase">
    Productos Destacados
  </h1>
  <div class="products">
          
  <article class="product-miniature js-product-miniature" data-id-product="1" data-id-product-attribute="1" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://neuracorestudio.com/bit-legends/mx/tshirts/1-1-faded-short-sleeves-tshirt.html#/1-tamano-s/13-color-naranja" class="thumbnail product-thumbnail">
          <img
            src = "http://neuracorestudio.com/bit-legends/1-home_default/faded-short-sleeves-tshirt.jpg"
            alt = "Faded Short Sleeves T-shirt"
            data-full-size-image-url = "http://neuracorestudio.com/bit-legends/1-large_default/faded-short-sleeves-tshirt.jpg"
          >
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a href="http://neuracorestudio.com/bit-legends/mx/tshirts/1-1-faded-short-sleeves-tshirt.html#/1-tamano-s/13-color-naranja">Faded Short Sleeves T-shirt</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Precio</span>
              <span itemprop="price" class="price">$19.15</span>

              

            
          </div>
              

      
        
      
    </div>

    
      <ul class="product-flags">
              </ul>
    

    <div class="highlighted-informations hidden-sm-down">
      
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Vista rápida
        </a>
      

      
                  <div class="variant-links">
      <a href="http://neuracorestudio.com/bit-legends/mx/tshirts/1-1-faded-short-sleeves-tshirt.html#/1-tamano-s/13-color-naranja"
       class="color"
       title="Naranja"
       
       style="background-color: #F39C11"           ><span class="sr-only">Naranja</span></a>
      <a href="http://neuracorestudio.com/bit-legends/mx/tshirts/1-2-faded-short-sleeves-tshirt.html#/1-tamano-s/14-color-azul"
       class="color"
       title="Azul"
       
       style="background-color: #5D9CEC"           ><span class="sr-only">Azul</span></a>
    <span class="js-count count"></span>
</div>
              
    </div>

  </article>


          
  <article class="product-miniature js-product-miniature" data-id-product="2" data-id-product-attribute="7" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://neuracorestudio.com/bit-legends/mx/blouses/2-7-blouse.html#/1-tamano-s/11-color-negro" class="thumbnail product-thumbnail">
          <img
            src = "http://neuracorestudio.com/bit-legends/7-home_default/blouse.jpg"
            alt = "Blouse"
            data-full-size-image-url = "http://neuracorestudio.com/bit-legends/7-large_default/blouse.jpg"
          >
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a href="http://neuracorestudio.com/bit-legends/mx/blouses/2-7-blouse.html#/1-tamano-s/11-color-negro">Blouse</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Precio</span>
              <span itemprop="price" class="price">$31.31</span>

              

            
          </div>
              

      
        
      
    </div>

    
      <ul class="product-flags">
              </ul>
    

    <div class="highlighted-informations hidden-sm-down">
      
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Vista rápida
        </a>
      

      
                  <div class="variant-links">
      <a href="http://neuracorestudio.com/bit-legends/mx/blouses/2-8-blouse.html#/1-tamano-s/8-color-blanco"
       class="color"
       title="Blanco"
       
       style="background-color: #ffffff"           ><span class="sr-only">Blanco</span></a>
      <a href="http://neuracorestudio.com/bit-legends/mx/blouses/2-7-blouse.html#/1-tamano-s/11-color-negro"
       class="color"
       title="Negro"
       
       style="background-color: #434A54"           ><span class="sr-only">Negro</span></a>
    <span class="js-count count"></span>
</div>
              
    </div>

  </article>


          
  <article class="product-miniature js-product-miniature" data-id-product="3" data-id-product-attribute="13" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://neuracorestudio.com/bit-legends/mx/casual-dresses/3-13-printed-dress.html#/1-tamano-s/13-color-naranja" class="thumbnail product-thumbnail">
          <img
            src = "http://neuracorestudio.com/bit-legends/8-home_default/printed-dress.jpg"
            alt = "Printed Dress"
            data-full-size-image-url = "http://neuracorestudio.com/bit-legends/8-large_default/printed-dress.jpg"
          >
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a href="http://neuracorestudio.com/bit-legends/mx/casual-dresses/3-13-printed-dress.html#/1-tamano-s/13-color-naranja">Printed Dress</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Precio</span>
              <span itemprop="price" class="price">$30.15</span>

              

            
          </div>
              

      
        
      
    </div>

    
      <ul class="product-flags">
              </ul>
    

    <div class="highlighted-informations hidden-sm-down">
      
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Vista rápida
        </a>
      

      
                  <div class="variant-links">
      <a href="http://neuracorestudio.com/bit-legends/mx/casual-dresses/3-13-printed-dress.html#/1-tamano-s/13-color-naranja"
       class="color"
       title="Naranja"
       
       style="background-color: #F39C11"           ><span class="sr-only">Naranja</span></a>
    <span class="js-count count"></span>
</div>
              
    </div>

  </article>


          
  <article class="product-miniature js-product-miniature" data-id-product="4" data-id-product-attribute="16" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://neuracorestudio.com/bit-legends/mx/evening-dresses/4-16-printed-dress.html#/1-tamano-s/7-color-beige" class="thumbnail product-thumbnail">
          <img
            src = "http://neuracorestudio.com/bit-legends/10-home_default/printed-dress.jpg"
            alt = "Printed Dress"
            data-full-size-image-url = "http://neuracorestudio.com/bit-legends/10-large_default/printed-dress.jpg"
          >
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a href="http://neuracorestudio.com/bit-legends/mx/evening-dresses/4-16-printed-dress.html#/1-tamano-s/7-color-beige">Printed Dress</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Precio</span>
              <span itemprop="price" class="price">$59.15</span>

              

            
          </div>
              

      
        
      
    </div>

    
      <ul class="product-flags">
              </ul>
    

    <div class="highlighted-informations hidden-sm-down">
      
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Vista rápida
        </a>
      

      
                  <div class="variant-links">
      <a href="http://neuracorestudio.com/bit-legends/mx/evening-dresses/4-16-printed-dress.html#/1-tamano-s/7-color-beige"
       class="color"
       title="Beige"
       
       style="background-color: #f5f5dc"           ><span class="sr-only">Beige</span></a>
      <a href="http://neuracorestudio.com/bit-legends/mx/evening-dresses/4-43-printed-dress.html#/1-tamano-s/24-color-rosa"
       class="color"
       title="Rosa"
       
       style="background-color: #FCCACD"           ><span class="sr-only">Rosa</span></a>
    <span class="js-count count"></span>
</div>
              
    </div>

  </article>


          
  <article class="product-miniature js-product-miniature" data-id-product="5" data-id-product-attribute="19" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://neuracorestudio.com/bit-legends/mx/summer-dresses/5-19-printed-summer-dress.html#/1-tamano-s/16-color-yellow" class="thumbnail product-thumbnail">
          <img
            src = "http://neuracorestudio.com/bit-legends/12-home_default/printed-summer-dress.jpg"
            alt = "Printed Summer Dress"
            data-full-size-image-url = "http://neuracorestudio.com/bit-legends/12-large_default/printed-summer-dress.jpg"
          >
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a href="http://neuracorestudio.com/bit-legends/mx/summer-dresses/5-19-printed-summer-dress.html#/1-tamano-s/16-color-yellow">Printed Summer Dress</a></h1>
        

        
                      <div class="product-price-and-shipping">
                              

                <span class="sr-only">Precio base</span>
                <span class="regular-price">$35.38</span>
                                  <span class="discount-percentage">-5%</span>
                              
              

              <span class="sr-only">Precio</span>
              <span itemprop="price" class="price">$33.61</span>

              

            
          </div>
              

      
        
      
    </div>

    
      <ul class="product-flags">
                  <li class="product-flag discount">Precio rebajado</li>
              </ul>
    

    <div class="highlighted-informations hidden-sm-down">
      
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Vista rápida
        </a>
      

      
                  <div class="variant-links">
      <a href="http://neuracorestudio.com/bit-legends/mx/summer-dresses/5-22-printed-summer-dress.html#/1-tamano-s/11-color-negro"
       class="color"
       title="Negro"
       
       style="background-color: #434A54"           ><span class="sr-only">Negro</span></a>
      <a href="http://neuracorestudio.com/bit-legends/mx/summer-dresses/5-21-printed-summer-dress.html#/1-tamano-s/13-color-naranja"
       class="color"
       title="Naranja"
       
       style="background-color: #F39C11"           ><span class="sr-only">Naranja</span></a>
      <a href="http://neuracorestudio.com/bit-legends/mx/summer-dresses/5-20-printed-summer-dress.html#/1-tamano-s/14-color-azul"
       class="color"
       title="Azul"
       
       style="background-color: #5D9CEC"           ><span class="sr-only">Azul</span></a>
      <a href="http://neuracorestudio.com/bit-legends/mx/summer-dresses/5-19-printed-summer-dress.html#/1-tamano-s/16-color-yellow"
       class="color"
       title="Yellow"
       
       style="background-color: #F1C40F"           ><span class="sr-only">Yellow</span></a>
    <span class="js-count count"></span>
</div>
              
    </div>

  </article>


          
  <article class="product-miniature js-product-miniature" data-id-product="6" data-id-product-attribute="31" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://neuracorestudio.com/bit-legends/mx/summer-dresses/6-31-printed-summer-dress.html#/1-tamano-s/16-color-yellow" class="thumbnail product-thumbnail">
          <img
            src = "http://neuracorestudio.com/bit-legends/16-home_default/printed-summer-dress.jpg"
            alt = "Printed Summer Dress"
            data-full-size-image-url = "http://neuracorestudio.com/bit-legends/16-large_default/printed-summer-dress.jpg"
          >
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a href="http://neuracorestudio.com/bit-legends/mx/summer-dresses/6-31-printed-summer-dress.html#/1-tamano-s/16-color-yellow">Printed Summer Dress</a></h1>
        

        
                      <div class="product-price-and-shipping">
              
              

              <span class="sr-only">Precio</span>
              <span itemprop="price" class="price">$35.38</span>

              

            
          </div>
              

      
        
      
    </div>

    
      <ul class="product-flags">
              </ul>
    

    <div class="highlighted-informations hidden-sm-down">
      
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Vista rápida
        </a>
      

      
                  <div class="variant-links">
      <a href="http://neuracorestudio.com/bit-legends/mx/summer-dresses/6-40-printed-summer-dress.html#/1-tamano-s/8-color-blanco"
       class="color"
       title="Blanco"
       
       style="background-color: #ffffff"           ><span class="sr-only">Blanco</span></a>
      <a href="http://neuracorestudio.com/bit-legends/mx/summer-dresses/6-31-printed-summer-dress.html#/1-tamano-s/16-color-yellow"
       class="color"
       title="Yellow"
       
       style="background-color: #F1C40F"           ><span class="sr-only">Yellow</span></a>
    <span class="js-count count"></span>
</div>
              
    </div>

  </article>


          
  <article class="product-miniature js-product-miniature" data-id-product="7" data-id-product-attribute="34" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
        <a href="http://neuracorestudio.com/bit-legends/mx/summer-dresses/7-34-printed-chiffon-dress.html#/1-tamano-s/16-color-yellow" class="thumbnail product-thumbnail">
          <img
            src = "http://neuracorestudio.com/bit-legends/20-home_default/printed-chiffon-dress.jpg"
            alt = "Printed Chiffon Dress"
            data-full-size-image-url = "http://neuracorestudio.com/bit-legends/20-large_default/printed-chiffon-dress.jpg"
          >
        </a>
      

      <div class="product-description">
        
          <h1 class="h3 product-title" itemprop="name"><a href="http://neuracorestudio.com/bit-legends/mx/summer-dresses/7-34-printed-chiffon-dress.html#/1-tamano-s/16-color-yellow">Printed Chiffon Dress</a></h1>
        

        
                      <div class="product-price-and-shipping">
                              

                <span class="sr-only">Precio base</span>
                <span class="regular-price">$23.78</span>
                                  <span class="discount-percentage">-20%</span>
                              
              

              <span class="sr-only">Precio</span>
              <span itemprop="price" class="price">$19.03</span>

              

            
          </div>
              

      
        
      
    </div>

    
      <ul class="product-flags">
                  <li class="product-flag discount">Precio rebajado</li>
              </ul>
    

    <div class="highlighted-informations hidden-sm-down">
      
        <a class="quick-view" href="#" data-link-action="quickview">
          <i class="material-icons search">&#xE8B6;</i> Vista rápida
        </a>
      

      
                  <div class="variant-links">
      <a href="http://neuracorestudio.com/bit-legends/mx/summer-dresses/7-37-printed-chiffon-dress.html#/1-tamano-s/15-color-verde"
       class="color"
       title="Verde"
       
       style="background-color: #A0D468"           ><span class="sr-only">Verde</span></a>
      <a href="http://neuracorestudio.com/bit-legends/mx/summer-dresses/7-34-printed-chiffon-dress.html#/1-tamano-s/16-color-yellow"
       class="color"
       title="Yellow"
       
       style="background-color: #F1C40F"           ><span class="sr-only">Yellow</span></a>
    <span class="js-count count"></span>
</div>
              
    </div>

  </article>


      </div>
  <a class="all-product-link float-xs-left float-md-right h4" href="http://neuracorestudio.com/bit-legends/mx/2-inicio">
    Todos los productos<i class="material-icons">&#xE315;</i>
  </a>
</section>
<?php }} ?>
