<?php /*%%SmartyHeaderCode:479975232599bb5dab29e29-03617504%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '906548e89c8c6025457ddaeffb1980a0c743b872' => 
    array (
      0 => 'module:ps_linklist/views/templates/hook/linkblock.tpl',
      1 => 1503374249,
      2 => 'module',
    ),
  ),
  'nocache_hash' => '479975232599bb5dab29e29-03617504',
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5baaa8a01145f8_87839009',
  'has_nocache_code' => false,
  'cache_lifetime' => 31536000,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5baaa8a01145f8_87839009')) {function content_5baaa8a01145f8_87839009($_smarty_tpl) {?><div class="col-md-4 links">
  <div class="row">
      <div class="col-md-6 wrapper">
      <h3 class="h3 hidden-sm-down">Productos</h3>
            <div class="title clearfix hidden-md-up" data-target="#footer_sub_menu_73534" data-toggle="collapse">
        <span class="h3">Productos</span>
        <span class="float-xs-right">
          <span class="navbar-toggler collapse-icons">
            <i class="material-icons add">&#xE313;</i>
            <i class="material-icons remove">&#xE316;</i>
          </span>
        </span>
      </div>
      <ul id="footer_sub_menu_73534" class="collapse">
                  <li>
            <a
                id="link-product-page-prices-drop-1"
                class="cms-page-link"
                href="http://neuracorestudio.com/bit-legends/mx/productos-rebajados"
                title="Productos en oferta">
              Ofertas
            </a>
          </li>
                  <li>
            <a
                id="link-product-page-new-products-1"
                class="cms-page-link"
                href="http://neuracorestudio.com/bit-legends/mx/novedades"
                title="Novedades">
              Nuevos productos
            </a>
          </li>
                  <li>
            <a
                id="link-product-page-best-sales-1"
                class="cms-page-link"
                href="http://neuracorestudio.com/bit-legends/mx/mas-vendidos"
                title="Los más vendidos">
              Los más vendidos
            </a>
          </li>
              </ul>
    </div>
      <div class="col-md-6 wrapper">
      <h3 class="h3 hidden-sm-down">Nuestra empresa</h3>
            <div class="title clearfix hidden-md-up" data-target="#footer_sub_menu_19821" data-toggle="collapse">
        <span class="h3">Nuestra empresa</span>
        <span class="float-xs-right">
          <span class="navbar-toggler collapse-icons">
            <i class="material-icons add">&#xE313;</i>
            <i class="material-icons remove">&#xE316;</i>
          </span>
        </span>
      </div>
      <ul id="footer_sub_menu_19821" class="collapse">
                  <li>
            <a
                id="link-cms-page-1-2"
                class="cms-page-link"
                href="http://neuracorestudio.com/bit-legends/mx/content/1-delivery"
                title="Our terms and conditions of delivery">
              Delivery
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-2-2"
                class="cms-page-link"
                href="http://neuracorestudio.com/bit-legends/mx/content/2-legal-notice"
                title="Legal notice">
              Legal Notice
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-3-2"
                class="cms-page-link"
                href="http://neuracorestudio.com/bit-legends/mx/content/3-terms-and-conditions-of-use"
                title="Our terms and conditions of use">
              Terms and conditions of use
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-4-2"
                class="cms-page-link"
                href="http://neuracorestudio.com/bit-legends/mx/content/4-about-us"
                title="Learn more about us">
              About us
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-5-2"
                class="cms-page-link"
                href="http://neuracorestudio.com/bit-legends/mx/content/5-secure-payment"
                title="Our secure payment method">
              Secure payment
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-contact-2"
                class="cms-page-link"
                href="http://neuracorestudio.com/bit-legends/mx/contactenos"
                title="Contáctenos">
              Contactar con nosotros
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-sitemap-2"
                class="cms-page-link"
                href="http://neuracorestudio.com/bit-legends/mx/Mapa del sitio"
                title="¿Perdido? Encuentre lo que está buscando">
              Mapa del sitio
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-stores-2"
                class="cms-page-link"
                href="http://neuracorestudio.com/bit-legends/mx/tiendas"
                title="">
              Tiendas
            </a>
          </li>
              </ul>
    </div>
    </div>
</div>
<?php }} ?>
